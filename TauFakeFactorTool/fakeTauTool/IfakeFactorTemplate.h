#ifndef fakeTauTool_IfakeFactorTemplate_H
#define fakeTauTool_IfakeFactorTemplate_H

#include "AsgTools/IAsgTool.h"
#include <vector>
#include <string>
#include <TH1.h>
#include <TTree.h>

using namespace std;

namespace fakeTauTool {
    
    class IfakeFactorTemplate : public virtual asg::IAsgTool
    {
    
        ASG_TOOL_INTERFACE( fakeTauTool::IfakeFactorTemplate )

        
        public:
            void getFakeFactors(const char*, int debugFlag = 0, int rebinFlag = 1, int rigidFlag = 0);
            
        private:
            std::vector<double> *getLumiScale(std::vector<const char*>*);
            std::vector<TH1F*> *getHisto(double*, int&, std::vector<std::string>*, std::string&, std::string&, const char*, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *getPTDistro(std::string&, double*, int&, std::vector<double>*, std::vector<std::string>*, const char*, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *getPTNormalization(std::vector<TH1F*>*, std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, std::string&, std::string&, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *getMultijetPTNormalization(std::vector<TH1F*>*, std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, std::string&, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *getWidthDistro(std::vector<std::vector<TH1F*>>*, std::vector<std::vector<TH1F*>>*, std::vector<std::vector<TH1F*>>*, std::vector<std::string>*, std::string&, double*, int&, vector<double>*, const char*, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *combineHistos(std::vector<std::vector<TH1F*>>*, std::vector<std::vector<TH1F*>>*, std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *combineGluonUnmatchedHistos(std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, int debugFlag = 0);
            std::vector<float>* doFit(TH1F*, TH1F*, TH1F*, std::string&, int debugFlag = 0, int rebinFlag = 1, int rigidFlag = 0);            
            std::vector<std::vector<float>> *getQuarkFractions(std::vector<TH1F*>*, std::vector<TH1F*>*, std::vector<TH1F*>*, std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, int rebinFlag = 1, int rigidFlag = 0);
            void interpolateFF(std::vector<TH1F*>*, std::vector<TH1F*>*, std::vector<std::vector<float>>*, double*, int&, std::string&,int debugFlag = 0);
            std::vector<std::string>* getDirectories(const char*);
            std::vector<std::string>* getCuts(std::string&);
            std::vector<int> getBinning(std::string&, const char*);
    };
}

#endif
