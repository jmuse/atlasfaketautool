#ifndef fakeTauTool_fakeFactorTemplate_H
#define fakeTauTool_fakeFactorTemplate_H

#include "fakeTauTool/IfakeFactorTemplate.h"
#include "AsgTools/AsgTool.h"
#include <vector>
#include <TH1.h>
#include <string>
#include <TTree.h>

namespace fakeTauTool {

    class fakeFactorTemplate  : public virtual IfakeFactorTemplate, public asg::AsgTool
    {
        ASG_TOOL_CLASS( fakeFactorTemplate, fakeTauTool::IfakeFactorTemplate )

        public:
            fakeFactorTemplate(const std::string& name);
            void getFakeFactors(const char*, int debugFlag = 0, int rebinFlag = 1, int rigidFlag = 0);
            
        private:
            std::vector<double> *getLumiScale(std::vector<const char*>*);
            std::vector<TH1F*> *getHisto(double*, int&, std::vector<std::string>*, std::string&, std::string&, const char*, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *getPTDistro(std::string&, double*, int&, std::vector<double>*, std::vector<std::string>*, const char*, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *getPTNormalization(std::vector<TH1F*>*, std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, std::string&, std::string&, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *getMultijetPTNormalization(std::vector<TH1F*>*, std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, std::string&, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *getWidthDistro(std::vector<std::vector<TH1F*>>*, std::vector<std::vector<TH1F*>>*, std::vector<std::vector<TH1F*>>*, std::vector<std::string>*, std::string&, double*, int&, vector<double>*, const char*, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *combineHistos(std::vector<std::vector<TH1F*>>*, std::vector<std::vector<TH1F*>>*, std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, int debugFlag = 0);
            std::vector<std::vector<TH1F*>> *combineGluonUnmatchedHistos(std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, int debugFlag = 0);
            std::vector<float>* doFit(TH1F*, TH1F*, TH1F*, std::string&, int debugFlag = 0, int rebinFlag = 1, int rigidFlag = 0);            
            std::vector<std::vector<float>> *getQuarkFractions(std::vector<TH1F*>*, std::vector<TH1F*>*, std::vector<TH1F*>*, std::vector<std::vector<TH1F*>>*, double*, int&, std::string&, int rebinFlag = 1, int rigidFlag = 0);
            void interpolateFF(std::vector<TH1F*>*, std::vector<TH1F*>*, std::vector<std::vector<float>>*, double*, int&, std::string&, int debugFlag = 0);
            std::vector<std::string>* getDirectories(const char*);
            std::vector<std::string>* getCuts(std::string&);
            std::vector<int> getBinning(std::string&, const char*);

            Long64_t nEntries;

            double totSumW;
            double mcChannelNumber;
            double kFactor;
            double xSection;
            double FilterEfficiency;
            double denom;
            double newLumi;
            double lumiScale;
            double b_mcChannelNumber;
            double maxBin;
            double minBin;
            double quarkContent;
            double gluonContent;
            double unmatchedContent;
            double regionContent;
            double minFineBin;
            double maxFineBin;
            double newScaleQuarkZmumu;
            double newScaleQuarkMultijet;
            double newScaleQuarkSample;
            double newScaleGluonZmumu;
            double newScaleGluonMultijet;
            double newScaleGluonSample;
            double newScaleUnmatchedZmumu;
            double newScaleUnmatchedMultijet;
            double newScaleUnmatchedSample;

            std::string cuts1;
            std::string cuts2;
            std::string cuts4;
            std::string cuts5;
            std::string gluonStr = "gluon";
            std::string quarkStr = "quark";
            std::string unmatchedStr = "unmatched";
            std::string min;
            std::string max;
            std::string maxStr;
            std::string minStr;
            std::string histoOIStr;
            std::string titleStr;
            std::string quarkWidthTitleStr;
            std::string gluonWidthTitleStr;
            std::string unmatchedWidthTitleStr;
            std::string quarkWidthNormTitleZmumuStr;
            std::string quarkPtTitleZmumuStr;
            std::string gluonWidthNormTitleZmumuStr;
            std::string gluonPtTitleZmumuStr;
            std::string unmatchedWidthNormTitleZmumuStr;
            std::string unmatchedPtTitleZmumuStr;
            std::string quarkWidthNormTitleMultijetStr;
            std::string quarkPtTitleMultijetStr;
            std::string gluonWidthNormTitleMultijetStr;
            std::string gluonPtTitleMultijetStr;
            std::string unmatchedWidthNormTitleMultijetStr;
            std::string unmatchedPtTitleMultijetStr;
            std::string quarkWidthNormTitleSampleStr;
            std::string quarkPtTitleSampleStr;
            std::string gluonWidthNormTitleSampleStr;
            std::string gluonPtTitleSampleStr;
            std::string unmatchedWidthNormTitleSampleStr;
            std::string unmatchedPtTitleSampleStr;
            std::string titleZmumuQuarkStr;
            std::string titleZmumuGluonStr;
            std::string titleZmumuUnmatchedStr;
            std::string titleMultijetQuarkStr;
            std::string titleMultijetGluonStr;
            std::string titleMultijetUnmatchedStr;
            std::string titleSampleQuarkStr;
            std::string titleSampleGluonStr;
            std::string titleSampleUnmatchedStr;
            std::string titleZmumuStr;
            std::string titleMultijetStr;
            std::string titleSampleStr;
            std::string Zmumu;
            std::string Multijet;
            std::string Sample;
            std::string pt;
            std::string ff;
            std::string width;
            std::string Wlnu;
            std::string Zll;
            std::string jetjet;

            int loopSize;
            int numBins;
            int numFineBins;
            int minInt;
            int maxInt;
            int pass_fail_bdt;
            int pass_trigger;

            float b_mc_weights;
            float weight;
            float zmumuFF;
            float multijetFF;
            float zmumuQuarkFrac;
            float multijetQuarkFrac;
            float sampleQuarkFrac;
            float quarkFF;
            float gluonFF;
            float outFF;

            const char* ZmumuFileName;
            const char* MultijetFileName;
            const char* WlnuFileName;
            const char* ZllFileName;
            const char* jetjetFileName;
            const char* titleOut;
            const char* directory;
            
    };
}

#endif
