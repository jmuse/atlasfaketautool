// System include(s):
#include <memory>
#include <cstdlib>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TString.h>
#include <TH1.h>
#include <vector>
#include <TTree.h>
#include <string>

#include "fakeTauTool/fakeFactorTemplate.h"

int main( int argc, char* argv[] ) {

    fakeTauTool::fakeFactorTemplate fakes("fakeFactor");
    
    const char* inFile = argv[1];
    
    int debugFlag = 0;
    int rebinFlag = 1;
    int rigidFlag = 0;
 
    for(int i = 2; i < argc; i++){
	std::string argvi(argv[i]);
	if(argvi == "-debugFlag"){
	    debugFlag = atoi(argv[i + 1]);
            std::string argv2(argv[i + 1]);
            if(argv2 == "true"){
                debugFlag = 1; 
            }
            if(argv2 == "false"){
                debugFlag = 0;
            }
	}
	if(argvi == "-rebin"){
	    rebinFlag = atoi(argv[i + 1]);
	}
	if(argvi == "-rigidFlag"){
	    rigidFlag = atoi(argv[i + 1]);
            std::string argv2(argv[i + 1]);
            if(argv2 == "true"){
                rigidFlag = 1;
            }
            if(argv2 == "false"){
                rigidFlag = 0;
            }
	}
    }

    fakes.getFakeFactors(inFile, debugFlag, rebinFlag, rigidFlag);
    
}
