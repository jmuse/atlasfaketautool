///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////         //// ///         //////////////////////////////////////////////////////////            
//////////  / /////// /////  //////////////////////////////////////////////////////////////
//////////  ////       ////  //////////////////////////////////////////////////////////////
////// ///  ///////// /////  /// //////////////////////////////////////////////////////////     
//////      //////// //////      //////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//                                   //////////////////////////////////////////////////////
//    ATLAS Fake Tau Tool            //////////////////////////////////////////////////////
//    Date: 05/05/2019               //////////////////////////////////////////////////////
//    Author: Joe Muse               //////////////////////////////////////////////////////
//    Email: joseph.m.muse@ou.edu    //////////////////////////////////////////////////////
//                                   //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////  

#include <fakeTauTool/fakeFactorTemplate.h>
#include <TObjArray.h>
#include <TFractionFitter.h>
#include <Fit/ParameterSettings.h>
#include <Fit/Fitter.h>
#include <TFile.h>
#include <TH1.h>
#include <array>
#include <TBranch.h>
#include <vector>
#include <TRandom3.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <string>
#include <iostream>
#include <AsgTools/MessageCheck.h>
#include "RooRealVar.h"
#include "RooConstVar.h"
#include "RooGaussian.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooDataSet.h"
#include "RooPlot.h"
#include <RooStats/HistFactory/Measurement.h>
#include <RooStats/HistFactory/Channel.h>
#include <RooStats/HistFactory/Sample.h>
#include <RooStats/HistFactory/MakeModelAndMeasurementsFast.h>
#include <RooStats/ModelConfig.h>
#include <RooWorkspace.h>
#include <RooMinimizer.h>
#include "AtlasUtils.C"
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "fakeTauTool/AtlasUtils.h"
#include "fakeTauTool/AtlasStyle.h"
#include "fakeTauTool/AtlasLabels.h"

namespace fakeTauTool{
    
    fakeFactorTemplate::fakeFactorTemplate (const std::string& name): asg::AsgTool( name ){
        
        TFile *outFile = new TFile("fakeTau.root", "RECREATE");
        outFile->Close();

    }

    std::vector<double>* fakeFactorTemplate::getLumiScale(vector<const char*> *inFileNames){

        //create vector of scales to weight input templates to cross section

        TChain chain("MetaData_Collection");

	int inFileNamessize = inFileNames->size();
        for(int i = 0; i < inFileNamessize; i++){
            chain.Add(inFileNames->at(i));
        }

        std::vector<double> *scaleVector = new std::vector<double>;

        nEntries = chain.GetEntries();

        chain.SetBranchStatus("*", 1);
        chain.SetBranchAddress("totSumW", &totSumW);
        chain.SetBranchAddress("mcChannelNumber", &mcChannelNumber);
        chain.SetBranchAddress("kFactor", &kFactor);
        chain.SetBranchAddress("xSection", &xSection);
        chain.SetBranchAddress("FilterEfficiency", &FilterEfficiency);

        for(Long64_t i = 0; i < nEntries;i++){
            chain.GetEntry(i);

            denom = kFactor * xSection * FilterEfficiency;
            newLumi = totSumW/denom;
            lumiScale = 1/newLumi;

            scaleVector->push_back(mcChannelNumber);
            scaleVector->push_back(lumiScale);
        }

        return scaleVector;

    }

    std::vector<TH1F*>* fakeFactorTemplate::getHisto(double *bins, int &numBins, std::vector<std::string> *inCuts, std::string &variable, std::string &region, const char* inFileName, int debugFlag){

        // get pT or width histo for samples used in interpolation process (i.e. zmumu, jetjet, or your sample of interest)

        std::vector<TH1F*> outHisto;
        std::vector<TH1F*> *cloneOutHisto = new std::vector<TH1F*>;

        TFile inFile(inFileName);

        for(int i = 0; i < numBins; i++){
            maxBin = bins[i + 1];
            minBin = bins[i];
            maxInt = maxBin;
            minInt = minBin;
            maxStr = std::to_string(maxInt);
            minStr = std::to_string(minInt);
            histoOIStr =  region + "_" + inCuts->at(1) + "_" + inCuts->at(2) + "_" + inCuts->at(3) + "_" + inCuts->at(4) + "_" + inCuts->at(5) + "_" + inCuts->at(6) + "/h_pt" + minStr + maxStr + "_" + variable;
            titleStr = region + "_" + inCuts->at(1) + "_" + minStr + maxStr + "_" + variable;

            TH1F *tempHisto = (TH1F*)inFile.Get(histoOIStr.c_str());
            TH1F *clone = (TH1F*)tempHisto->Clone("temp");

            clone->SetName(titleStr.c_str());
            clone->SetTitle(titleStr.c_str());

            outHisto.push_back(clone);
        }

        TFile outFile("fakeTau.root", "UPDATE");

	int outHistosize = outHisto.size();
        for(int i = 0; i < outHistosize; i++){
            TH1F *clone = (TH1F*)outHisto.at(i)->Clone();
            clone->SetDirectory(0);
            cloneOutHisto->push_back(clone);
            if(debugFlag == 1){
                outHisto.at(i)->Write();
            }
        }

        outFile.Close();
        inFile.Close();

        return cloneOutHisto;

    }
    
    std::vector<std::vector<TH1F*>>* fakeFactorTemplate::getPTDistro(std::string &sample, double *bins, int &numBins, std::vector<double> *scaleVector, std::vector<std::string> *inCuts, const char* inFileName, int debugFlag){

        // obtain pT distribution of input template samples, these are used to reweight against

        TChain chain("FakeTaus_Collection");
        chain.Add(inFileName);

        std::vector<TH1F> quark_histo;
        std::vector<TH1F> gluon_histo;
        std::vector<TH1F> unmatched_histo;
        std::vector<TH1F*> clone_quark_histos;
        std::vector<TH1F*> clone_gluon_histos;
        std::vector<TH1F*> clone_unmatched_histos;
        std::vector<std::vector<TH1F*>> *cloneOutHisto = new std::vector<std::vector<TH1F*>>;
        std::vector<float> *b_pt = nullptr;
        std::vector<bool> *quark = nullptr;
        std::vector<bool> *gluon = nullptr;
        std::vector<bool> *unmatched = nullptr;
        std::vector<bool> *prong = nullptr;
        std::vector<bool> *trig = nullptr;
        std::vector<bool> *bdt = nullptr;
        std::vector<bool> *bdtscore = nullptr;
        std::vector<bool> *jvt = nullptr;

        cuts1 = inCuts->at(1) + "_";
        cuts2 = inCuts->at(2) + "_";
        cuts4 = inCuts->at(4) + "_";
        cuts5 = inCuts->at(5) + "_";

        nEntries = chain.GetEntries();
        loopSize = scaleVector->size()/2;

        chain.SetBranchStatus("*", 0);
        chain.SetBranchStatus("b_mc_weights", 1);
        chain.SetBranchStatus("b_mcChannelNumber", 1);
        chain.SetBranchStatus("b_pt", 1);
        chain.SetBranchStatus("q_", 1);
        chain.SetBranchStatus("g_", 1);
        chain.SetBranchStatus("u_", 1);
        chain.SetBranchStatus(cuts1.c_str(), 1);
        chain.SetBranchStatus(cuts2.c_str(), 1);
        chain.SetBranchStatus(cuts4.c_str(), 1);
        chain.SetBranchStatus(cuts5.c_str(), 1);
        chain.SetBranchStatus("trig_", 1);

        chain.SetBranchAddress("b_mc_weights", &b_mc_weights);
        chain.SetBranchAddress("b_mcChannelNumber", &b_mcChannelNumber);
        chain.SetBranchAddress("b_pt", &b_pt);
        chain.SetBranchAddress("q_", &quark);
        chain.SetBranchAddress("g_", &gluon);
        chain.SetBranchAddress("u_", &unmatched);
        chain.SetBranchAddress(cuts1.c_str(), &prong);
        chain.SetBranchAddress(cuts2.c_str(), &bdt);
        chain.SetBranchAddress(cuts4.c_str(), &bdtscore);
        chain.SetBranchAddress(cuts5.c_str(), &jvt);
        chain.SetBranchAddress("trig_", &trig);

        pass_fail_bdt = 1;
        pass_trigger = 1;

        if(inCuts->at(3) == "pass"){
            pass_fail_bdt = 0;
        }

        if(inCuts->at(3) == "trig1"){
            pass_trigger = 0;
        }

        for(int i = 0; i < numBins; i++){
            maxBin = bins[i + 1];
            minBin = bins[i];
            numFineBins = maxBin - minBin;
            minInt = minBin;
            maxInt = maxBin;

            min = std::to_string(minInt);
            max = std::to_string(maxInt);
            quarkStr = sample + "_" + quarkStr + "_" + cuts1 + min + max;
            gluonStr = sample + "_" + gluonStr + "_" + cuts1 + min + max;
            unmatchedStr = sample + "_" + unmatchedStr + "_" + cuts1 + min + max;

            TH1F tempQuarkHisto(quarkStr.c_str(), quarkStr.c_str(), numFineBins, minBin, maxBin);
            TH1F tempGluonHisto(gluonStr.c_str(), gluonStr.c_str(), numFineBins, minBin, maxBin);
            TH1F tempUnmatchedHisto(unmatchedStr.c_str(), unmatchedStr.c_str(), numFineBins, minBin, maxBin);

            tempQuarkHisto.Sumw2();
            tempGluonHisto.Sumw2();
            tempUnmatchedHisto.Sumw2();

            quark_histo.push_back(tempQuarkHisto);
            gluon_histo.push_back(tempGluonHisto);
            unmatched_histo.push_back(tempUnmatchedHisto);
        }

        for(Long64_t i = 0; i < nEntries; i++){
            chain.GetEntry(i);

            if(b_pt->size() == 0) continue;
            if(b_mcChannelNumber == 364700) continue;

            weight = 1;

            for(int j = 0; j < loopSize; j++){
                if(b_mcChannelNumber == scaleVector->at(j)){
                    weight = scaleVector->at(j + 1) * b_mc_weights;
                }
            }

	    int ptsize = b_pt->size();
            for(int j = 0; j < ptsize; j++){
                if(prong->at(j) == 0) continue;
                if(bdt->at(j) == pass_fail_bdt) continue;
                if(bdtscore->at(j) == 1) continue;
                if(trig->at(j) != pass_trigger) continue;
                for(int binInt = 0; binInt < numBins; binInt++){
                    maxBin = bins[binInt + 1];
                    minBin = bins[binInt];

                    if((b_pt->at(j) >= minBin) && (b_pt->at(j) < maxBin)){
                        if(quark->at(j) == 1){
                            quark_histo.at(binInt).Fill(b_pt->at(j), weight);
                        }
                        if(gluon->at(j) == 1){
                            gluon_histo.at(binInt).Fill(b_pt->at(j), weight);
                        }
                        if(unmatched->at(j) == 1){
                            unmatched_histo.at(binInt).Fill(b_pt->at(j), weight);                      
                        } 
                    }
                }                           
            }

            b_pt->clear();
            quark->clear();
            prong->clear();
            gluon->clear();
            unmatched->clear();
            bdt->clear();
            bdtscore->clear();
            jvt->clear();
            trig->clear();
        }

        TFile outFile("fakeTau.root", "UPDATE");

	int quark_histosize = quark_histo.size();
        for(int i = 0; i < quark_histosize; i++){
            TH1F *clone = (TH1F*)quark_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos.push_back(clone);
            if(debugFlag == 1){
                quark_histo.at(i).Write();
            }
        }

	int gluon_histosize = gluon_histo.size();
        for(int i = 0; i < gluon_histosize; i++){
            TH1F *clone = (TH1F*)gluon_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos.push_back(clone);
            if(debugFlag == 1){
                gluon_histo.at(i).Write();
            }
        }

	int unmatched_histosize = unmatched_histo.size();
        for(int i = 0; i < unmatched_histosize; i++){
            TH1F *clone = (TH1F*)unmatched_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos.push_back(clone);
            if(debugFlag == 1){
                unmatched_histo.at(i).Write();
            }
        }

        cloneOutHisto->push_back(clone_quark_histos);
        cloneOutHisto->push_back(clone_gluon_histos);
        cloneOutHisto->push_back(clone_unmatched_histos);

        outFile.Close();

        return cloneOutHisto;
        
    }

    std::vector<std::vector<TH1F*>>* fakeFactorTemplate::getPTNormalization(std::vector<TH1F*> *inRegion, std::vector<std::vector<TH1F*>> *inTemplate, double *bins, int &numBins, std::string &prong, std::string &sample, std::string &region, int debugFlag){

        //normalize pT distribution of input template to zmumu/region of interest (done seperate from jetjet due to binning differences)->1GeV bins

        std::vector<TH1F> quark_histo;
        std::vector<TH1F> gluon_histo;
        std::vector<TH1F> unmatched_histo;
        std::vector<TH1F*> clone_quark_histos;
        std::vector<TH1F*> clone_gluon_histos;
        std::vector<TH1F*> clone_unmatched_histos;
        std::vector<std::vector<TH1F*>> *cloneOutHisto = new std::vector<std::vector<TH1F*>>;

        for(int i = 0; i < numBins; i++){
            minBin = bins[i];
            maxBin = bins[i + 1];
            numFineBins = maxBin - minBin;
            minInt = minBin;
            maxInt = maxBin;
            minStr = std::to_string(minInt);
            maxStr = std::to_string(maxInt);
            quarkStr = region + "_" + prong + "_" + minStr + maxStr + "_pt_normFactors_" + sample + "_quark";
            gluonStr = region + "_" + prong + "_" + minStr + maxStr + "_pt_normFactors_" + sample + "_gluon";
            unmatchedStr = region + "_" + prong + "_" + minStr + maxStr + "_pt_normFactors_" + sample + "_unmatched";

            TH1F tempProngQuark(quarkStr.c_str(), quarkStr.c_str(), numFineBins, minBin, maxBin);
            TH1F tempProngGluon(gluonStr.c_str(), gluonStr.c_str(), numFineBins, minBin, maxBin);
            TH1F tempProngUnmatched(unmatchedStr.c_str(), unmatchedStr.c_str(), numFineBins, minBin, maxBin);

            tempProngQuark.Sumw2();
            tempProngGluon.Sumw2();
            tempProngUnmatched.Sumw2();

            quark_histo.push_back(tempProngQuark);
            gluon_histo.push_back(tempProngGluon);
            unmatched_histo.push_back(tempProngUnmatched);
        }

        for(int i = 0; i < numBins; i++){
            minBin = bins[i];
            maxBin = bins[i + 1];
            numFineBins = maxBin - minBin;

            for(int k = 0; k < numFineBins; k++){
                quarkContent = inTemplate->at(0).at(i)->GetBinContent(k + 1);
                gluonContent = inTemplate->at(1).at(i)->GetBinContent(k + 1);
                unmatchedContent = inTemplate->at(2).at(i)->GetBinContent(k + 1);
                regionContent = inRegion->at(i)->GetBinContent(k + 1);

                if(quarkContent == 0){
                    quark_histo.at(i).SetBinContent(k + 1, regionContent);
                }

                else{
                    quark_histo.at(i).SetBinContent(k + 1, regionContent/quarkContent);
                }

                if(gluonContent == 0){
                    gluon_histo.at(i).SetBinContent(k + 1, regionContent);
                }

                else{
                    gluon_histo.at(i).SetBinContent(k + 1, regionContent/gluonContent);
                }
                if(unmatchedContent == 0){
                    unmatched_histo.at(i).SetBinContent(k + 1, regionContent);
                }

                else{
                    unmatched_histo.at(i).SetBinContent(k + 1, regionContent/unmatchedContent);
                }
            }
        }

        TFile outFile("fakeTau.root", "UPDATE");

	int quark_histosize = quark_histo.size();
        for(int i = 0; i < quark_histosize; i++){
            TH1F *clone = (TH1F*)quark_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos.push_back(clone);
            if(debugFlag == 1){
                quark_histo.at(i).Write();
            }
        }

	int gluon_histosize = gluon_histo.size();
        for(int i = 0; i < gluon_histosize; i++){
            TH1F *clone = (TH1F*)gluon_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos.push_back(clone);
            if(debugFlag == 1){
                gluon_histo.at(i).Write();
            }
        }

	int unmatched_histosize = unmatched_histo.size();
        for(int i = 0; i < unmatched_histosize; i++){
            TH1F *clone = (TH1F*)unmatched_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos.push_back(clone);
            if(debugFlag == 1){
                unmatched_histo.at(i).Write();
            }
        }

        cloneOutHisto->push_back(clone_quark_histos);
        cloneOutHisto->push_back(clone_gluon_histos);
        cloneOutHisto->push_back(clone_unmatched_histos);

        outFile.Close();

        return cloneOutHisto;

    }

    std::vector<std::vector<TH1F*>>* fakeFactorTemplate::getMultijetPTNormalization(std::vector<TH1F*> *inRegion, std::vector<std::vector<TH1F*>> *inTemplate, double *bins, int &numBins, std::string &prong, std::string &sample, int debugFlag){

        //normalize pT distribution of input template to jetjet sample (done seperate from multijet due to binning differences)->1GeV bins

        std::vector<TH1F> quark_histo;
        std::vector<TH1F> gluon_histo;
        std::vector<TH1F> unmatched_histo;
        std::vector<TH1F*> clone_quark_histos;
        std::vector<TH1F*> clone_gluon_histos;
        std::vector<TH1F*> clone_unmatched_histos;
        std::vector<std::vector<TH1F*>> *cloneOutHisto = new std::vector<std::vector<TH1F*>>;

        for(int i = 0; i < numBins; i++){
            minBin = bins[i];
            maxBin = bins[i + 1];
            numFineBins = maxBin - minBin;
            minInt = minBin;
            maxInt = maxBin;
            minStr = std::to_string(minInt);
            maxStr = std::to_string(maxInt);
            quarkStr = "multijet_" + prong + "_" + minStr + maxStr + "_pt_normFactors_" + sample + "_quark";
            gluonStr = "multijet_" + prong + "_" + minStr + maxStr + "_pt_normFactors_" + sample + "_gluon";
            unmatchedStr = "multijet_" + prong + "_" + minStr + maxStr + "_pt_normFactors_" + sample + "_unmatched";

            TH1F tempProngQuark(quarkStr.c_str(), quarkStr.c_str(), numFineBins, minBin, maxBin);
            TH1F tempProngGluon(gluonStr.c_str(), gluonStr.c_str(), numFineBins, minBin, maxBin);
            TH1F tempProngUnmatched(unmatchedStr.c_str(), unmatchedStr.c_str(), numFineBins, minBin, maxBin);

            tempProngQuark.Sumw2();
            tempProngGluon.Sumw2();
            tempProngUnmatched.Sumw2();

            quark_histo.push_back(tempProngQuark);
            gluon_histo.push_back(tempProngGluon);
            unmatched_histo.push_back(tempProngUnmatched);
        }

        for(int i = 0; i < numBins; i++){
            minBin = bins[i];
            maxBin = bins[i + 1];
            numFineBins = maxBin - minBin;

            for(int k = 0; k < numFineBins; k++){
                quarkContent = inTemplate->at(0).at(i)->GetBinContent(k + 1);
                gluonContent = inTemplate->at(1).at(i)->GetBinContent(k + 1);
                unmatchedContent = inTemplate->at(2).at(i)->GetBinContent(k + 1);
                regionContent = inRegion->at(i)->GetBinContent(minBin + k + 1);

                if(quarkContent == 0){
                    quark_histo.at(i).SetBinContent(k + 1, regionContent);
                }

                else{
                    quark_histo.at(i).SetBinContent(k + 1, regionContent/quarkContent);
                }

                if(gluonContent == 0){
                    gluon_histo.at(i).SetBinContent(k + 1, regionContent);
                }

                else{
                    gluon_histo.at(i).SetBinContent(k + 1, regionContent/gluonContent);
                }
                if(unmatchedContent == 0){
                    unmatched_histo.at(i).SetBinContent(k + 1, regionContent);
                }

                else{
                    unmatched_histo.at(i).SetBinContent(k + 1, regionContent/unmatchedContent);
                }
            }
        }

        TFile outFile("fakeTau.root", "UPDATE");

	int quark_histosize = quark_histo.size();
        for(int i = 0; i < quark_histosize; i++){
            TH1F *clone = (TH1F*)quark_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos.push_back(clone);
            if(debugFlag == 1){
                quark_histo.at(i).Write();
            }
        }

	int gluon_histosize = gluon_histo.size();
        for(int i = 0; i < gluon_histosize; i++){
            TH1F *clone = (TH1F*)gluon_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos.push_back(clone);
            if(debugFlag == 1){
                gluon_histo.at(i).Write();
            }
        }

	int unmatched_histosize = unmatched_histo.size();
        for(int i = 0; i < unmatched_histosize; i++){
            TH1F *clone = (TH1F*)unmatched_histo.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos.push_back(clone);
            if(debugFlag == 1){
                unmatched_histo.at(i).Write();
            }
        }

        cloneOutHisto->push_back(clone_quark_histos);
        cloneOutHisto->push_back(clone_gluon_histos);
        cloneOutHisto->push_back(clone_unmatched_histos);

        outFile.Close();

        return cloneOutHisto;

    }

    std::vector<std::vector<TH1F*>>* fakeFactorTemplate::getWidthDistro(std::vector<std::vector<TH1F*>> *inZmumuNormalized, std::vector<std::vector<TH1F*>> *inMultijetNormalized, std::vector<std::vector<TH1F*>> *inRegionNormalized, std::vector<std::string> *inCuts, std::string &sample, double *bins, int &numBins, vector<double> *scaleVector, const char* inFileName, int debugFlag){

        // obtain width distribution of input template samples, reweight these to pT

        TChain chain("FakeTaus_Collection");
        chain.Add(inFileName);

        std::vector<TH1F> quark_histo_width;
        std::vector<TH1F> gluon_histo_width;
        std::vector<TH1F> unmatched_histo_width;
        std::vector<TH1F> quark_histo_width_norm_zmumu;
        std::vector<TH1F> gluon_histo_width_norm_zmumu;
        std::vector<TH1F> unmatched_histo_width_norm_zmumu;
        std::vector<TH1F> quark_histo_pt_norm_zmumu;
        std::vector<TH1F> gluon_histo_pt_norm_zmumu;
        std::vector<TH1F> unmatched_histo_pt_norm_zmumu;
        std::vector<TH1F> quark_histo_width_norm_multijet;
        std::vector<TH1F> gluon_histo_width_norm_multijet;
        std::vector<TH1F> unmatched_histo_width_norm_multijet;
        std::vector<TH1F> quark_histo_pt_norm_multijet;
        std::vector<TH1F> gluon_histo_pt_norm_multijet;
        std::vector<TH1F> unmatched_histo_pt_norm_multijet;
        std::vector<TH1F> quark_histo_width_norm_sample;
        std::vector<TH1F> gluon_histo_width_norm_sample;
        std::vector<TH1F> unmatched_histo_width_norm_sample;
        std::vector<TH1F> quark_histo_pt_norm_sample;
        std::vector<TH1F> gluon_histo_pt_norm_sample;
        std::vector<TH1F> unmatched_histo_pt_norm_sample;
        std::vector<TH1F*> clone_quark_histos_zmumu;
        std::vector<TH1F*> clone_gluon_histos_zmumu;
        std::vector<TH1F*> clone_unmatched_histos_zmumu;
        std::vector<TH1F*> clone_quark_histos_multijet;
        std::vector<TH1F*> clone_gluon_histos_multijet;
        std::vector<TH1F*> clone_unmatched_histos_multijet;
        std::vector<TH1F*> clone_quark_histos_sample;
        std::vector<TH1F*> clone_gluon_histos_sample;
        std::vector<TH1F*> clone_unmatched_histos_sample;
        std::vector<std::vector<TH1F*>> *cloneOutHisto = new std::vector<std::vector<TH1F*>>;

        std::vector<float> *b_pt = nullptr;
        std::vector<float> *b_width = nullptr;
        std::vector<bool> *quark = nullptr;
        std::vector<bool> *gluon = nullptr;
        std::vector<bool> *unmatched = nullptr;
        std::vector<bool> *prong = nullptr;
        std::vector<bool> *trig = nullptr;
        std::vector<bool> *bdt = nullptr;
        std::vector<bool> *bdtscore = nullptr;
        std::vector<bool> *jvt = nullptr;

        cuts1 = inCuts->at(1) + "_";
        cuts2 = inCuts->at(2) + "_";
        cuts4 = inCuts->at(4) + "_";
        cuts5 = inCuts->at(5) + "_";

        nEntries = chain.GetEntries();
        loopSize = scaleVector->size()/2;

        chain.SetBranchStatus("*", 0);
        chain.SetBranchStatus("b_mc_weights", 1);
        chain.SetBranchStatus("b_mcChannelNumber", 1);
        chain.SetBranchStatus("b_pt", 1);
        chain.SetBranchStatus("b_width", 1);
        chain.SetBranchStatus("q_", 1);
        chain.SetBranchStatus("g_", 1);
        chain.SetBranchStatus("u_", 1);
        chain.SetBranchStatus(cuts1.c_str(), 1);
        chain.SetBranchStatus(cuts2.c_str(), 1);
        chain.SetBranchStatus(cuts4.c_str(), 1);
        chain.SetBranchStatus(cuts5.c_str(), 1);
        chain.SetBranchStatus("trig_", 1);

        chain.SetBranchAddress("b_mc_weights", &b_mc_weights);
        chain.SetBranchAddress("b_mcChannelNumber", &b_mcChannelNumber);
        chain.SetBranchAddress("b_pt", &b_pt);
        chain.SetBranchAddress("b_width", &b_width);
        chain.SetBranchAddress("q_", &quark);
        chain.SetBranchAddress("g_", &gluon);
        chain.SetBranchAddress("u_", &unmatched);
        chain.SetBranchAddress(cuts1.c_str(), &prong);
        chain.SetBranchAddress(cuts2.c_str(), &bdt);
        chain.SetBranchAddress(cuts4.c_str(), &bdtscore);
        chain.SetBranchAddress(cuts5.c_str(), &jvt);
        chain.SetBranchAddress("trig_", &trig);

        pass_fail_bdt = 1;
        pass_trigger = 1;

        if(inCuts->at(3) == "pass"){
            pass_fail_bdt = 0;
        }

        if(inCuts->at(3) == "trig1"){
            pass_trigger = 0;
        }

        for(int i = 0; i < numBins; i++){
            maxBin = bins[i + 1];
            minBin = bins[i];
            numFineBins = maxBin - minBin;
            minInt = minBin;
            maxInt = maxBin;
            min = std::to_string(minInt);
            max = std::to_string(maxInt);
            quarkWidthTitleStr = sample + "_" + quarkStr + "_" + cuts1 + min + max + "_width";
            gluonWidthTitleStr = sample + "_" + gluonStr + "_" + cuts1 + min + max + "_width";
            unmatchedWidthTitleStr = sample + "_" + unmatchedStr + "_" + cuts1 + min + max + "_width";
            quarkWidthNormTitleZmumuStr = sample + "_" + quarkStr + "_" + cuts1 + min + max + "_width_Normalized_Zmumu";
            quarkPtTitleZmumuStr = sample + "_" + quarkStr + "_" + cuts1 + min + max + "_pt_Normalized_Zmumu";
            gluonWidthNormTitleZmumuStr = sample + "_" + gluonStr + "_" + cuts1 + min + max + "_width_Normalized_Zmumu";
            gluonPtTitleZmumuStr = sample + "_" + gluonStr + "_" + cuts1 + min + max + "_pt_Normalized_Zmumu";
            unmatchedWidthNormTitleZmumuStr = sample + "_" + unmatchedStr + "_" + cuts1 + min + max + "_width_Normalized_Zmumu";
            unmatchedPtTitleZmumuStr = sample + "_" + unmatchedStr + "_" + cuts1 + min + max + "_pt_Normalized_Zmumu";
            quarkWidthNormTitleMultijetStr = sample + "_" + quarkStr + "_" + cuts1 + min + max + "_width_Normalized_Multijet";
            quarkPtTitleMultijetStr = sample + "_" + quarkStr + "_" + cuts1 + min + max + "_pt_Normalized_Multijet";
            gluonWidthNormTitleMultijetStr = sample + "_" + gluonStr + "_" + cuts1 + min + max + "_width_Normalized_Multijet";
            gluonPtTitleMultijetStr = sample + "_" + gluonStr + "_" + cuts1 + min + max + "_pt_Normalized_Multijet";
            unmatchedWidthNormTitleMultijetStr = sample + "_" + unmatchedStr + "_" + cuts1 + min + max + "_width_Normalized_Multijet";
            unmatchedPtTitleMultijetStr = sample + "_" + unmatchedStr + "_" + cuts1 + min + max + "_pt_Normalized_Multijet";
            quarkWidthNormTitleSampleStr = sample + "_" + quarkStr + "_" + cuts1 + min + max + "_width_Normalized_Sample";
            quarkPtTitleSampleStr = sample + "_" + quarkStr + "_" + cuts1 + min + max + "_pt_Normalized_Sample";
            gluonWidthNormTitleSampleStr = sample + "_" + gluonStr + "_" + cuts1 + min + max + "_width_Normalized_Sample";
            gluonPtTitleSampleStr = sample + "_" + gluonStr + "_" + cuts1 + min + max + "_pt_Normalized_Sample";
            unmatchedWidthNormTitleSampleStr = sample + "_" + unmatchedStr + "_" + cuts1 + min + max + "_width_Normalized_Sample";
            unmatchedPtTitleSampleStr = sample + "_" + unmatchedStr + "_" + cuts1 + min + max + "_pt_Normalized_Sample";

            TH1F quarkWidthTemp(quarkWidthTitleStr.c_str(), quarkWidthTitleStr.c_str(), 30, 0, 0.3);
            TH1F gluonWidthTemp(gluonWidthTitleStr.c_str(), gluonWidthTitleStr.c_str(), 30, 0, 0.3);
            TH1F unmatchedWidthTemp(unmatchedWidthTitleStr.c_str(), unmatchedWidthTitleStr.c_str(), 30, 0, 0.3);
            TH1F quarkWidthNormZmumuTemp(quarkWidthNormTitleZmumuStr.c_str(), quarkWidthNormTitleZmumuStr.c_str(), 30, 0, 0.3);
            TH1F quarkPtZmumuTemp(quarkPtTitleZmumuStr.c_str(), quarkPtTitleZmumuStr.c_str(), numFineBins, minBin, maxBin);
            TH1F gluonWidthNormZmumuTemp(gluonWidthNormTitleZmumuStr.c_str(), gluonWidthNormTitleZmumuStr.c_str(), 30, 0, 0.3);
            TH1F gluonPtZmumuTemp(gluonPtTitleZmumuStr.c_str(), gluonPtTitleZmumuStr.c_str(), numFineBins, minBin, maxBin);
            TH1F unmatchedWidthNormZmumuTemp(unmatchedWidthNormTitleZmumuStr.c_str(), unmatchedWidthNormTitleZmumuStr.c_str(), 30, 0, 0.3);
            TH1F unmatchedPtZmumuTemp(unmatchedPtTitleZmumuStr.c_str(), unmatchedPtTitleZmumuStr.c_str(), numFineBins, minBin, maxBin);
            TH1F quarkWidthNormMultijetTemp(quarkWidthNormTitleMultijetStr.c_str(), quarkWidthNormTitleMultijetStr.c_str(), 30, 0, 0.3);
            TH1F quarkPtMultijetTemp(quarkPtTitleMultijetStr.c_str(), quarkPtTitleMultijetStr.c_str(), numFineBins, minBin, maxBin);
            TH1F gluonWidthNormMultijetTemp(gluonWidthNormTitleMultijetStr.c_str(), gluonWidthNormTitleMultijetStr.c_str(), 30, 0, 0.3);
            TH1F gluonPtMultijetTemp(gluonPtTitleMultijetStr.c_str(), gluonPtTitleMultijetStr.c_str(), numFineBins, minBin, maxBin);
            TH1F unmatchedWidthNormMultijetTemp(unmatchedWidthNormTitleMultijetStr.c_str(), unmatchedWidthNormTitleMultijetStr.c_str(), 30, 0, 0.3);
            TH1F unmatchedPtMultijetTemp(unmatchedPtTitleMultijetStr.c_str(), unmatchedPtTitleMultijetStr.c_str(), numFineBins, minBin, maxBin);
            TH1F quarkWidthNormSampleTemp(quarkWidthNormTitleSampleStr.c_str(), quarkWidthNormTitleSampleStr.c_str(), 30, 0, 0.3);
            TH1F quarkPtSampleTemp(quarkPtTitleSampleStr.c_str(), quarkPtTitleSampleStr.c_str(), numFineBins, minBin, maxBin);
            TH1F gluonWidthNormSampleTemp(gluonWidthNormTitleSampleStr.c_str(), gluonWidthNormTitleSampleStr.c_str(), 30, 0, 0.3);
            TH1F gluonPtSampleTemp(gluonPtTitleSampleStr.c_str(), gluonPtTitleSampleStr.c_str(), numFineBins, minBin, maxBin);
            TH1F unmatchedWidthNormSampleTemp(unmatchedWidthNormTitleSampleStr.c_str(), unmatchedWidthNormTitleSampleStr.c_str(), 30, 0, 0.3);
            TH1F unmatchedPtSampleTemp(unmatchedPtTitleSampleStr.c_str(), unmatchedPtTitleSampleStr.c_str(), numFineBins, minBin, maxBin);

            quarkWidthTemp.Sumw2();
            gluonWidthTemp.Sumw2();
            unmatchedWidthTemp.Sumw2();
            quarkWidthNormZmumuTemp.Sumw2();
            quarkPtZmumuTemp.Sumw2();
            gluonWidthNormZmumuTemp.Sumw2();
            gluonPtZmumuTemp.Sumw2();
            unmatchedWidthNormZmumuTemp.Sumw2();
            unmatchedPtZmumuTemp.Sumw2();
            quarkWidthNormMultijetTemp.Sumw2();
            quarkPtMultijetTemp.Sumw2();
            gluonWidthNormMultijetTemp.Sumw2();
            gluonPtMultijetTemp.Sumw2();
            unmatchedWidthNormMultijetTemp.Sumw2();
            unmatchedPtMultijetTemp.Sumw2();
            quarkWidthNormSampleTemp.Sumw2();
            quarkPtSampleTemp.Sumw2();
            gluonWidthNormSampleTemp.Sumw2();
            gluonPtSampleTemp.Sumw2();
            unmatchedWidthNormSampleTemp.Sumw2();
            unmatchedPtSampleTemp.Sumw2();

            quark_histo_width.push_back(quarkWidthTemp);
            gluon_histo_width.push_back(gluonWidthTemp);
            unmatched_histo_width.push_back(unmatchedWidthTemp);
            quark_histo_width_norm_zmumu.push_back(quarkWidthNormZmumuTemp);
            gluon_histo_width_norm_zmumu.push_back(gluonWidthNormZmumuTemp);
            unmatched_histo_width_norm_zmumu.push_back(unmatchedWidthNormZmumuTemp);
            quark_histo_pt_norm_zmumu.push_back(quarkPtZmumuTemp);
            gluon_histo_pt_norm_zmumu.push_back(gluonPtZmumuTemp);
            unmatched_histo_pt_norm_zmumu.push_back(unmatchedPtZmumuTemp);
            quark_histo_width_norm_multijet.push_back(quarkWidthNormMultijetTemp);
            gluon_histo_width_norm_multijet.push_back(gluonWidthNormMultijetTemp);
            unmatched_histo_width_norm_multijet.push_back(unmatchedWidthNormMultijetTemp);
            quark_histo_pt_norm_multijet.push_back(quarkPtMultijetTemp);
            gluon_histo_pt_norm_multijet.push_back(gluonPtMultijetTemp);
            unmatched_histo_pt_norm_multijet.push_back(unmatchedPtMultijetTemp);
            quark_histo_width_norm_sample.push_back(quarkWidthNormSampleTemp);
            gluon_histo_width_norm_sample.push_back(gluonWidthNormSampleTemp);
            unmatched_histo_width_norm_sample.push_back(unmatchedWidthNormSampleTemp);
            quark_histo_pt_norm_sample.push_back(quarkPtSampleTemp);
            gluon_histo_pt_norm_sample.push_back(gluonPtSampleTemp);
            unmatched_histo_pt_norm_sample.push_back(unmatchedPtSampleTemp);
        }

        for(Long64_t i = 0; i < nEntries; i++){
            chain.GetEntry(i);

            if(b_pt->size() == 0) continue;
            if(b_mcChannelNumber == 364700) continue;

            weight = 1;

            for(int j = 0; j < loopSize; j++){
                if(b_mcChannelNumber == scaleVector->at(j)){
                    weight = scaleVector->at(j + 1) * b_mc_weights;
                }
            }
	
	    int ptsize = b_pt->size();
            for(int j = 0; j < ptsize; j++){
                if(prong->at(j) == 0) continue;
                if(bdt->at(j) == pass_fail_bdt) continue;
                if(bdtscore->at(j) == 1) continue;
                if(trig->at(j) != pass_trigger) continue;
                for(int binInt = 0; binInt < numBins; binInt++){
                    maxBin = bins[binInt + 1];
                    minBin = bins[binInt];
                    numFineBins = maxBin - minBin;

                    for(int k = 0; k < numFineBins; k++){
                        minFineBin = minBin + k;
                        maxFineBin = minBin + k + 1;
                        newScaleQuarkZmumu = inZmumuNormalized->at(0).at(binInt)->GetBinContent(k + 1);
                        newScaleQuarkMultijet = inMultijetNormalized->at(0).at(binInt)->GetBinContent(k + 1);
                        newScaleQuarkSample = inRegionNormalized->at(0).at(binInt)->GetBinContent(k + 1);
                        newScaleGluonZmumu = inZmumuNormalized->at(1).at(binInt)->GetBinContent(k + 1);
                        newScaleGluonMultijet = inMultijetNormalized->at(1).at(binInt)->GetBinContent(k + 1);
                        newScaleGluonSample = inRegionNormalized->at(1).at(binInt)->GetBinContent(k + 1);
                        newScaleUnmatchedZmumu = inZmumuNormalized->at(2).at(binInt)->GetBinContent(k + 1);
                        newScaleUnmatchedMultijet = inMultijetNormalized->at(2).at(binInt)->GetBinContent(k + 1);
                        newScaleUnmatchedSample = inRegionNormalized->at(2).at(binInt)->GetBinContent(k + 1);

                        if((b_pt->at(j) >= minFineBin) && (b_pt->at(j) < maxFineBin)){
                            if(quark->at(j) == 1){
                                quark_histo_width_norm_zmumu.at(binInt).Fill(b_width->at(j), weight * newScaleQuarkZmumu);                                     
                                quark_histo_width_norm_multijet.at(binInt).Fill(b_width->at(j), weight * newScaleQuarkMultijet); 
                                quark_histo_width_norm_sample.at(binInt).Fill(b_width->at(j), weight * newScaleQuarkSample); 
                                quark_histo_width.at(binInt).Fill(b_width->at(j), weight); 
                                quark_histo_pt_norm_zmumu.at(binInt).Fill(b_pt->at(j), weight * newScaleQuarkZmumu);                                      
                                quark_histo_pt_norm_multijet.at(binInt).Fill(b_pt->at(j), weight * newScaleQuarkMultijet);  
                                quark_histo_pt_norm_sample.at(binInt).Fill(b_pt->at(j), weight * newScaleQuarkSample); 
                            }
                            if(gluon->at(j) == 1){
                                gluon_histo_width_norm_zmumu.at(binInt).Fill(b_width->at(j), weight * newScaleGluonZmumu); 
                                gluon_histo_width_norm_multijet.at(binInt).Fill(b_width->at(j), weight * newScaleGluonMultijet); 
                                gluon_histo_width_norm_sample.at(binInt).Fill(b_width->at(j), weight * newScaleGluonSample); 
                                gluon_histo_width.at(binInt).Fill(b_width->at(j), weight); 
                                gluon_histo_pt_norm_zmumu.at(binInt).Fill(b_pt->at(j), weight * newScaleGluonZmumu);  
                                gluon_histo_pt_norm_multijet.at(binInt).Fill(b_pt->at(j), weight * newScaleGluonMultijet);  
                                gluon_histo_pt_norm_sample.at(binInt).Fill(b_pt->at(j), weight * newScaleGluonSample); 
                            }
                            if(unmatched->at(j) == 1){
                                unmatched_histo_width_norm_zmumu.at(binInt).Fill(b_width->at(j), weight * newScaleUnmatchedZmumu); 
                                unmatched_histo_width_norm_multijet.at(binInt).Fill(b_width->at(j), weight * newScaleUnmatchedMultijet); 
                                unmatched_histo_width_norm_sample.at(binInt).Fill(b_width->at(j), weight * newScaleUnmatchedSample);
                                unmatched_histo_width.at(binInt).Fill(b_width->at(j), weight); 
                                unmatched_histo_pt_norm_zmumu.at(binInt).Fill(b_pt->at(j), weight * newScaleUnmatchedZmumu);  
                                unmatched_histo_pt_norm_multijet.at(binInt).Fill(b_pt->at(j), weight * newScaleUnmatchedMultijet);
                                unmatched_histo_pt_norm_sample.at(binInt).Fill(b_pt->at(j), weight * newScaleUnmatchedSample);    
                            } 
                        }
                    }
                }                           
            } 

            b_pt->clear();
            quark->clear();
            unmatched->clear();
            prong->clear();
            gluon->clear();
            bdt->clear();
            bdtscore->clear();
            jvt->clear();
            trig->clear();

        }

        TFile outFile("fakeTau.root", "UPDATE");

	int quark_histo_width_norm_zmumusize = quark_histo_width_norm_zmumu.size();
        for(int i = 0; i < quark_histo_width_norm_zmumusize; i++){
            TH1F *clone = (TH1F*)quark_histo_width_norm_zmumu.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_zmumu.push_back(clone);
            if(debugFlag == 1){
                quark_histo_width_norm_zmumu.at(i).Write();
            }
        }

	int gluon_histo_width_norm_zmumusize = gluon_histo_width_norm_zmumu.size();
        for(int i = 0; i < gluon_histo_width_norm_zmumusize; i++){
            TH1F *clone = (TH1F*)gluon_histo_width_norm_zmumu.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_zmumu.push_back(clone);
            if(debugFlag == 1){
                gluon_histo_width_norm_zmumu.at(i).Write();
            }
        }

	int unmatched_histo_width_norm_zmumusize = unmatched_histo_width_norm_zmumu.size();
        for(int i = 0; i < unmatched_histo_width_norm_zmumusize; i++){
            TH1F *clone = (TH1F*)unmatched_histo_width_norm_zmumu.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos_zmumu.push_back(clone);
            if(debugFlag == 1){
                unmatched_histo_width_norm_zmumu.at(i).Write();
            }
        }

	int quark_histo_width_norm_multijetsize = quark_histo_width_norm_multijet.size();
        for(int i = 0; i < quark_histo_width_norm_multijetsize; i++){
            TH1F *clone = (TH1F*)quark_histo_width_norm_multijet.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_multijet.push_back(clone);
            if(debugFlag == 1){
                quark_histo_width_norm_multijet.at(i).Write();
            }
        }

	int gluon_histo_width_norm_multijetsize = gluon_histo_width_norm_multijet.size();
        for(int i = 0; i < gluon_histo_width_norm_multijetsize; i++){
            TH1F *clone = (TH1F*)gluon_histo_width_norm_multijet.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_multijet.push_back(clone);
            if(debugFlag == 1){
                gluon_histo_width_norm_multijet.at(i).Write();
            }
        }

	int unmatched_histo_width_norm_multijetsize = unmatched_histo_width_norm_multijet.size();
        for(int i = 0; i < unmatched_histo_width_norm_multijetsize; i++){
            TH1F *clone = (TH1F*)unmatched_histo_width_norm_multijet.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos_multijet.push_back(clone);
            if(debugFlag == 1){
                unmatched_histo_width_norm_multijet.at(i).Write();
            }
        }

	int quark_histo_width_norm_samplesize = quark_histo_width_norm_sample.size();
        for(int i = 0; i < quark_histo_width_norm_samplesize; i++){
            TH1F *clone = (TH1F*)quark_histo_width_norm_sample.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_sample.push_back(clone);
            if(debugFlag == 1){
                quark_histo_width_norm_sample.at(i).Write();
            }
        }

	int gluon_histo_width_norm_samplesize = gluon_histo_width_norm_sample.size();
        for(int i = 0; i < gluon_histo_width_norm_samplesize; i++){
            TH1F *clone = (TH1F*)gluon_histo_width_norm_sample.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_sample.push_back(clone);
            if(debugFlag == 1){
                gluon_histo_width_norm_sample.at(i).Write();
            }
        }

	int unmatched_histo_width_norm_samplesize = unmatched_histo_width_norm_sample.size();
        for(int i = 0; i < unmatched_histo_width_norm_samplesize; i++){
            TH1F *clone = (TH1F*)unmatched_histo_width_norm_sample.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos_sample.push_back(clone);
            if(debugFlag == 1){
                unmatched_histo_width_norm_sample.at(i).Write();
            }
        }

        if(debugFlag == 1){
	    int quark_histo_pt_norm_zmumusize = quark_histo_pt_norm_zmumu.size();
            for(int i = 0; i < quark_histo_pt_norm_zmumusize; i++){
                quark_histo_pt_norm_zmumu.at(i).Write();
            }

	    int gluon_histo_pt_norm_zmumusize = gluon_histo_pt_norm_zmumu.size();
            for(int i = 0; i < gluon_histo_pt_norm_zmumusize; i++){
                gluon_histo_pt_norm_zmumu.at(i).Write();
            }

	    int unmatched_histo_pt_norm_zmumusize = unmatched_histo_pt_norm_zmumu.size();
            for(int i = 0; i < unmatched_histo_pt_norm_zmumusize; i++){
                unmatched_histo_pt_norm_zmumu.at(i).Write();
            }
	
	    int quark_histo_pt_norm_multijetsize = quark_histo_pt_norm_multijet.size();
            for(int i = 0; i < quark_histo_pt_norm_multijetsize; i++){
                quark_histo_pt_norm_multijet.at(i).Write();
            }

	    int gluon_histo_pt_norm_multijetsize = gluon_histo_pt_norm_multijet.size();
            for(int i = 0; i < gluon_histo_pt_norm_multijetsize; i++){
                gluon_histo_pt_norm_multijet.at(i).Write();
            }

	    int unmatched_histo_pt_norm_multijetsize = unmatched_histo_pt_norm_multijet.size();
            for(int i = 0; i < unmatched_histo_pt_norm_multijetsize; i++){
                unmatched_histo_pt_norm_multijet.at(i).Write();
            }

	    int quark_histo_pt_norm_samplesize = quark_histo_pt_norm_sample.size();
            for(int i = 0; i < quark_histo_pt_norm_samplesize; i++){
                quark_histo_pt_norm_sample.at(i).Write();
            }

	    int gluon_histo_pt_norm_samplesize = gluon_histo_pt_norm_sample.size();
            for(int i = 0; i < gluon_histo_pt_norm_samplesize; i++){
                gluon_histo_pt_norm_sample.at(i).Write();
            }

	    int unmatched_histo_pt_norm_samplesize = unmatched_histo_pt_norm_sample.size();
            for(int i = 0; i < unmatched_histo_pt_norm_samplesize; i++){
                unmatched_histo_pt_norm_sample.at(i).Write();
            }

	    int quark_histo_widthsize = quark_histo_width.size();
            for(int i = 0; i < quark_histo_widthsize; i++){
                quark_histo_width.at(i).Write();
            }
	
	    int gluon_histo_widthsize = gluon_histo_width.size();
            for(int i = 0; i < gluon_histo_widthsize; i++){
                gluon_histo_width.at(i).Write();
            }

	    int unmatched_histo_widthsize = unmatched_histo_width.size();
            for(int i = 0; i < unmatched_histo_widthsize; i++){
                unmatched_histo_width.at(i).Write();
            }
        }

        cloneOutHisto->push_back(clone_quark_histos_zmumu);
        cloneOutHisto->push_back(clone_gluon_histos_zmumu);
        cloneOutHisto->push_back(clone_unmatched_histos_zmumu);
        cloneOutHisto->push_back(clone_quark_histos_multijet);
        cloneOutHisto->push_back(clone_gluon_histos_multijet);
        cloneOutHisto->push_back(clone_unmatched_histos_multijet);
        cloneOutHisto->push_back(clone_quark_histos_sample);
        cloneOutHisto->push_back(clone_gluon_histos_sample);
        cloneOutHisto->push_back(clone_unmatched_histos_sample);

        outFile.Close();

        return cloneOutHisto;

    }

    std::vector<std::vector<TH1F*>>* fakeFactorTemplate::combineHistos(std::vector<std::vector<TH1F*>> *inWlnu, std::vector<std::vector<TH1F*>> *inZll, std::vector<std::vector<TH1F*>> *inJetjet, double *bins, int &numBins, std::string &prong, int debugFlag){

        // combine quark/gluon/unmatched templates from various samples

        std::vector<TH1F> zmumuQuarkHistos;
        std::vector<TH1F> multijetQuarkHistos;
        std::vector<TH1F> sampleQuarkHistos;
        std::vector<TH1F> zmumuGluonHistos;
        std::vector<TH1F> multijetGluonHistos;
        std::vector<TH1F> sampleGluonHistos;
        std::vector<TH1F> zmumuUnmatchedHistos;
        std::vector<TH1F> multijetUnmatchedHistos;
        std::vector<TH1F> sampleUnmatchedHistos;
        std::vector<TH1F*> clone_quark_histos_zmumu;
        std::vector<TH1F*> clone_gluon_histos_zmumu;
        std::vector<TH1F*> clone_unmatched_histos_zmumu;
        std::vector<TH1F*> clone_quark_histos_multijet;
        std::vector<TH1F*> clone_gluon_histos_multijet;
        std::vector<TH1F*> clone_unmatched_histos_multijet;
        std::vector<TH1F*> clone_quark_histos_sample;
        std::vector<TH1F*> clone_gluon_histos_sample;
        std::vector<TH1F*> clone_unmatched_histos_sample;
        std::vector<std::vector<TH1F*>> *cloneOutHisto = new std::vector<std::vector<TH1F*>>;

        for(int i = 0; i < numBins; i++){            
            minBin = bins[i];
            maxBin = bins[i + 1];
            minInt = minBin;
            maxInt = maxBin;
            minStr = std::to_string(minInt);
            maxStr = std::to_string(maxInt);
            titleZmumuQuarkStr = "zmumu_quarkTemplates_alltogether_" + prong + "_" + minStr + maxStr;
            titleZmumuGluonStr = "zmumu_gluonTemplates_alltogether_" + prong + "_" + minStr + maxStr;
            titleZmumuUnmatchedStr = "zmumu_unmatchedTemplates_alltogether_" + prong + "_" + minStr + maxStr;
            titleMultijetQuarkStr = "multijet_quarkTemplates_alltogether_" + prong + "_" + minStr + maxStr;
            titleMultijetGluonStr = "multijet_gluonTemplates_alltogether_" + prong + "_" + minStr + maxStr;
            titleMultijetUnmatchedStr = "multijet_unmatchedTemplates_alltogether_" + prong + "_" + minStr + maxStr;
            titleSampleQuarkStr = "sample_quarkTemplates_alltogether_" + prong + "_" + minStr + maxStr;
            titleSampleGluonStr = "sample_gluonTemplates_alltogether_" + prong + "_" + minStr + maxStr;
            titleSampleUnmatchedStr = "sample_unmatchedTemplates_alltogether_" + prong + "_" + minStr + maxStr;

            TH1F zmumuQuark(titleZmumuQuarkStr.c_str(), titleZmumuQuarkStr.c_str(), 30, 0, 0.3);
            TH1F zmumuGluon(titleZmumuGluonStr.c_str(), titleZmumuGluonStr.c_str(), 30, 0, 0.3);
            TH1F zmumuUnmatched(titleZmumuUnmatchedStr.c_str(), titleZmumuUnmatchedStr.c_str(), 30, 0, 0.3);
            TH1F multijetQuark(titleMultijetQuarkStr.c_str(), titleMultijetQuarkStr.c_str(), 30, 0, 0.3);
            TH1F multijetGluon(titleMultijetGluonStr.c_str(), titleMultijetGluonStr.c_str(), 30, 0, 0.3);
            TH1F multijetUnmatched(titleMultijetUnmatchedStr.c_str(), titleMultijetUnmatchedStr.c_str(), 30, 0, 0.3);
            TH1F sampleQuark(titleSampleQuarkStr.c_str(), titleSampleQuarkStr.c_str(), 30, 0, 0.3);
            TH1F sampleGluon(titleSampleGluonStr.c_str(), titleSampleGluonStr.c_str(), 30, 0, 0.3);
            TH1F sampleUnmatched(titleSampleUnmatchedStr.c_str(), titleSampleUnmatchedStr.c_str(), 30, 0, 0.3);

            zmumuQuark.Sumw2();
            zmumuGluon.Sumw2();
            zmumuUnmatched.Sumw2();
            multijetQuark.Sumw2();
            multijetGluon.Sumw2(); 
            multijetUnmatched.Sumw2(); 
            sampleQuark.Sumw2();
            sampleGluon.Sumw2();
            sampleUnmatched.Sumw2();

            zmumuQuarkHistos.push_back(zmumuQuark);
            zmumuGluonHistos.push_back(zmumuGluon);
            zmumuUnmatchedHistos.push_back(zmumuUnmatched);
            multijetQuarkHistos.push_back(multijetQuark);
            multijetGluonHistos.push_back(multijetGluon);
            multijetUnmatchedHistos.push_back(multijetUnmatched);
            sampleQuarkHistos.push_back(sampleQuark);
            sampleGluonHistos.push_back(sampleGluon);
            sampleUnmatchedHistos.push_back(sampleUnmatched);
        }

        for(int i = 0; i < numBins; i++){
            zmumuQuarkHistos.at(i).Add(inWlnu->at(0).at(i));
            zmumuQuarkHistos.at(i).Add(inZll->at(0).at(i));
            zmumuQuarkHistos.at(i).Add(inJetjet->at(0).at(i));
            zmumuGluonHistos.at(i).Add(inWlnu->at(1).at(i));
            zmumuGluonHistos.at(i).Add(inZll->at(1).at(i));
            zmumuGluonHistos.at(i).Add(inJetjet->at(1).at(i));
            zmumuUnmatchedHistos.at(i).Add(inWlnu->at(2).at(i));
            zmumuUnmatchedHistos.at(i).Add(inZll->at(2).at(i));
            zmumuUnmatchedHistos.at(i).Add(inJetjet->at(2).at(i));
            multijetQuarkHistos.at(i).Add(inWlnu->at(3).at(i));
            multijetQuarkHistos.at(i).Add(inZll->at(3).at(i));
            multijetQuarkHistos.at(i).Add(inJetjet->at(3).at(i));
            multijetGluonHistos.at(i).Add(inWlnu->at(4).at(i));
            multijetGluonHistos.at(i).Add(inZll->at(4).at(i));
            multijetGluonHistos.at(i).Add(inJetjet->at(4).at(i));
            multijetUnmatchedHistos.at(i).Add(inWlnu->at(5).at(i));
            multijetUnmatchedHistos.at(i).Add(inZll->at(5).at(i));
            multijetUnmatchedHistos.at(i).Add(inJetjet->at(5).at(i));
            sampleQuarkHistos.at(i).Add(inWlnu->at(6).at(i));
            sampleQuarkHistos.at(i).Add(inZll->at(6).at(i));
            sampleQuarkHistos.at(i).Add(inJetjet->at(6).at(i));
            sampleGluonHistos.at(i).Add(inWlnu->at(7).at(i));
            sampleGluonHistos.at(i).Add(inZll->at(7).at(i));
            sampleGluonHistos.at(i).Add(inJetjet->at(7).at(i));
            sampleUnmatchedHistos.at(i).Add(inWlnu->at(8).at(i));
            sampleUnmatchedHistos.at(i).Add(inZll->at(8).at(i));
            sampleUnmatchedHistos.at(i).Add(inJetjet->at(8).at(i));
        }

        TFile outFile("fakeTau.root", "UPDATE");

	int zmumuQuarkHistossize = zmumuQuarkHistos.size();
        for(int i = 0; i < zmumuQuarkHistossize; i++){
            TH1F *clone = (TH1F*)zmumuQuarkHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_zmumu.push_back(clone);
            if(debugFlag == 1){
                zmumuQuarkHistos.at(i).Write();
            }
        }

	int zmumuGluonHistossize = zmumuGluonHistos.size();
        for(int i = 0; i < zmumuGluonHistossize; i++){
            TH1F *clone = (TH1F*)zmumuGluonHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_zmumu.push_back(clone);
            if(debugFlag == 1){
                zmumuGluonHistos.at(i).Write();
            }
        }

	int zmumuUnmatchedHistossize = zmumuUnmatchedHistos.size();
        for(int i = 0; i < zmumuUnmatchedHistossize; i++){
            TH1F *clone = (TH1F*)zmumuUnmatchedHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos_zmumu.push_back(clone);
            if(debugFlag == 1){
                zmumuUnmatchedHistos.at(i).Write();
            }
        }

	int multijetQuarkHistossize = multijetQuarkHistos.size();
        for(int i = 0; i < multijetQuarkHistossize; i++){
            TH1F *clone = (TH1F*)multijetQuarkHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_multijet.push_back(clone);
            if(debugFlag == 1){
                multijetQuarkHistos.at(i).Write();
            }
        }

	int multijetGluonHistossize = multijetGluonHistos.size();
        for(int i = 0; i < multijetGluonHistossize; i++){
            TH1F *clone = (TH1F*)multijetGluonHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_multijet.push_back(clone);
            if(debugFlag == 1){
                multijetGluonHistos.at(i).Write();
            }
        }

	int multijetUnmatchedHistossize = multijetUnmatchedHistos.size();
        for(int i = 0; i < multijetUnmatchedHistossize; i++){
            TH1F *clone = (TH1F*)multijetUnmatchedHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos_multijet.push_back(clone);
            if(debugFlag == 1){
                multijetUnmatchedHistos.at(i).Write();
            }
        }

	int sampleQuarkHistossize = sampleQuarkHistos.size();
        for(int i = 0; i < sampleQuarkHistossize; i++){
            TH1F *clone = (TH1F*)sampleQuarkHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_sample.push_back(clone);
            if(debugFlag == 1){
                sampleQuarkHistos.at(i).Write();
            }
        }

	int sampleGluonHistossize = sampleGluonHistos.size();
        for(int i = 0; i < sampleGluonHistossize; i++){
            TH1F *clone = (TH1F*)sampleGluonHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_sample.push_back(clone);
            if(debugFlag == 1){
                sampleGluonHistos.at(i).Write();
            }
        }

	int sampleUnmatchedHistossize = sampleUnmatchedHistos.size();
        for(int i = 0; i < sampleUnmatchedHistossize; i++){
            TH1F *clone = (TH1F*)sampleUnmatchedHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_unmatched_histos_sample.push_back(clone);
            if(debugFlag == 1){
                sampleUnmatchedHistos.at(i).Write();
            }
        }

        cloneOutHisto->push_back(clone_quark_histos_zmumu);
        cloneOutHisto->push_back(clone_gluon_histos_zmumu);
        cloneOutHisto->push_back(clone_unmatched_histos_zmumu);
        cloneOutHisto->push_back(clone_quark_histos_multijet);
        cloneOutHisto->push_back(clone_gluon_histos_multijet);
        cloneOutHisto->push_back(clone_unmatched_histos_multijet);
        cloneOutHisto->push_back(clone_quark_histos_sample);
        cloneOutHisto->push_back(clone_gluon_histos_sample);
        cloneOutHisto->push_back(clone_unmatched_histos_sample);

        outFile.Close();

        return cloneOutHisto;

    }

    std::vector<std::vector<TH1F*>>* fakeFactorTemplate::combineGluonUnmatchedHistos(std::vector<std::vector<TH1F*>> *inHistos, double *bins, int &numBins, std::string &prong, int debugFlag){

        // combine gluon/unmatched templates from combined templates

        std::vector<TH1F> zmumuQuarkHistos;
        std::vector<TH1F> multijetQuarkHistos;
        std::vector<TH1F> sampleQuarkHistos;
        std::vector<TH1F> zmumuGluonHistos;
        std::vector<TH1F> multijetGluonHistos;
        std::vector<TH1F> sampleGluonHistos;
        std::vector<TH1F> zmumuUnmatchedHistos;
        std::vector<TH1F> multijetUnmatchedHistos;
        std::vector<TH1F> sampleUnmatchedHistos;
        std::vector<TH1F*> clone_quark_histos_zmumu;
        std::vector<TH1F*> clone_gluon_histos_zmumu;
        std::vector<TH1F*> clone_unmatched_histos_zmumu;
        std::vector<TH1F*> clone_quark_histos_multijet;
        std::vector<TH1F*> clone_gluon_histos_multijet;
        std::vector<TH1F*> clone_unmatched_histos_multijet;
        std::vector<TH1F*> clone_quark_histos_sample;
        std::vector<TH1F*> clone_gluon_histos_sample;
        std::vector<TH1F*> clone_unmatched_histos_sample;
        std::vector<std::vector<TH1F*>> *cloneOutHisto = new std::vector<std::vector<TH1F*>>;

        for(int i = 0; i < numBins; i++){            
            minBin = bins[i];
            maxBin = bins[i + 1];
            minInt = minBin;
            maxInt = maxBin;
            minStr = std::to_string(minInt);
            maxStr = std::to_string(maxInt);
            titleZmumuQuarkStr = "zmumu_quarkTemplates_alltogether_gluonUnmatchedCombined_" + prong + "_" + minStr + maxStr;
            titleZmumuGluonStr = "zmumu_gluonTemplates_alltogether_gluonUnmatchedCombined_" + prong + "_" + minStr + maxStr;
            titleMultijetQuarkStr = "multijet_quarkTemplates_alltogether_gluonUnmatchedCombined_" + prong + "_" + minStr + maxStr;
            titleMultijetGluonStr = "multijet_gluonTemplates_alltogether_gluonUnmatchedCombined_" + prong + "_" + minStr + maxStr;
            titleSampleQuarkStr = "sample_quarkTemplates_alltogether_gluonUnmatchedCombined_" + prong + "_" + minStr + maxStr;
            titleSampleGluonStr = "sample_gluonTemplates_alltogether_gluonUnmatchedCombined_" + prong + "_" + minStr + maxStr;

            TH1F zmumuQuark(titleZmumuQuarkStr.c_str(), titleZmumuQuarkStr.c_str(), 30, 0, 0.3);
            TH1F zmumuGluon(titleZmumuGluonStr.c_str(), titleZmumuGluonStr.c_str(), 30, 0, 0.3);
            TH1F zmumuUnmatched(titleZmumuUnmatchedStr.c_str(), titleZmumuUnmatchedStr.c_str(), 30, 0, 0.3);
            TH1F multijetQuark(titleMultijetQuarkStr.c_str(), titleMultijetQuarkStr.c_str(), 30, 0, 0.3);
            TH1F multijetGluon(titleMultijetGluonStr.c_str(), titleMultijetGluonStr.c_str(), 30, 0, 0.3);
            TH1F multijetUnmatched(titleMultijetUnmatchedStr.c_str(), titleMultijetUnmatchedStr.c_str(), 30, 0, 0.3);
            TH1F sampleQuark(titleSampleQuarkStr.c_str(), titleSampleQuarkStr.c_str(), 30, 0, 0.3);
            TH1F sampleGluon(titleSampleGluonStr.c_str(), titleSampleGluonStr.c_str(), 30, 0, 0.3);
            TH1F sampleUnmatched(titleSampleUnmatchedStr.c_str(), titleSampleUnmatchedStr.c_str(), 30, 0, 0.3);

            zmumuQuark.Sumw2();
            zmumuGluon.Sumw2();
            zmumuUnmatched.Sumw2();
            multijetQuark.Sumw2();
            multijetGluon.Sumw2(); 
            multijetUnmatched.Sumw2(); 
            sampleQuark.Sumw2();
            sampleGluon.Sumw2();
            sampleUnmatched.Sumw2();

            zmumuQuarkHistos.push_back(zmumuQuark);
            zmumuGluonHistos.push_back(zmumuGluon);
            zmumuUnmatchedHistos.push_back(zmumuUnmatched);
            multijetQuarkHistos.push_back(multijetQuark);
            multijetGluonHistos.push_back(multijetGluon);
            multijetUnmatchedHistos.push_back(multijetUnmatched);
            sampleQuarkHistos.push_back(sampleQuark);
            sampleGluonHistos.push_back(sampleGluon);
            sampleUnmatchedHistos.push_back(sampleUnmatched);
        }

        for(int i = 0; i < numBins; i++){
            zmumuQuarkHistos.at(i).Add(inHistos->at(0).at(i));
            zmumuGluonHistos.at(i).Add(inHistos->at(1).at(i));
            zmumuGluonHistos.at(i).Add(inHistos->at(2).at(i));
            multijetQuarkHistos.at(i).Add(inHistos->at(3).at(i));
            multijetGluonHistos.at(i).Add(inHistos->at(4).at(i));
            multijetGluonHistos.at(i).Add(inHistos->at(5).at(i));
            sampleQuarkHistos.at(i).Add(inHistos->at(6).at(i));
            sampleGluonHistos.at(i).Add(inHistos->at(7).at(i));
            sampleGluonHistos.at(i).Add(inHistos->at(8).at(i));
        }

        TFile outFile("fakeTau.root", "UPDATE");

	int zmumuQuarkHistossize = zmumuQuarkHistos.size();
        for(int i = 0; i < zmumuQuarkHistossize; i++){
            TH1F *clone = (TH1F*)zmumuQuarkHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_zmumu.push_back(clone);
            if(debugFlag == 1){
                zmumuQuarkHistos.at(i).Write();
            }
        }

	int zmumuGluonHistossize = zmumuGluonHistos.size();
        for(int i = 0; i < zmumuGluonHistossize; i++){
            TH1F *clone = (TH1F*)zmumuGluonHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_zmumu.push_back(clone);
            if(debugFlag == 1){            
                zmumuGluonHistos.at(i).Write();
            }
        }

	int multijetQuarkHistossize = multijetQuarkHistos.size();
        for(int i = 0; i < multijetQuarkHistossize; i++){
            TH1F *clone = (TH1F*)multijetQuarkHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_multijet.push_back(clone);
            if(debugFlag == 1){
                multijetQuarkHistos.at(i).Write();
            }
        }

	int multijetGluonHistossize = multijetGluonHistos.size();
        for(int i = 0; i < multijetGluonHistossize; i++){
            TH1F *clone = (TH1F*)multijetGluonHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_multijet.push_back(clone);
            if(debugFlag == 1){
                multijetGluonHistos.at(i).Write();
            }
        }

	int sampleQuarkHistossize = sampleQuarkHistos.size();
        for(int i = 0; i < sampleQuarkHistossize; i++){
            TH1F *clone = (TH1F*)sampleQuarkHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_quark_histos_sample.push_back(clone);
            if(debugFlag == 1){
                sampleQuarkHistos.at(i).Write();
            }
        }

	int sampleGluonHistossize = sampleGluonHistos.size();
        for(int i = 0; i < sampleGluonHistossize; i++){
            TH1F *clone = (TH1F*)sampleGluonHistos.at(i).Clone();
            clone->SetDirectory(0);
            clone_gluon_histos_sample.push_back(clone);
            if(debugFlag == 1){
                sampleGluonHistos.at(i).Write();
            }
        }

        cloneOutHisto->push_back(clone_quark_histos_zmumu);
        cloneOutHisto->push_back(clone_gluon_histos_zmumu);
        cloneOutHisto->push_back(clone_quark_histos_multijet);
        cloneOutHisto->push_back(clone_gluon_histos_multijet);
        cloneOutHisto->push_back(clone_quark_histos_sample);
        cloneOutHisto->push_back(clone_gluon_histos_sample);

        outFile.Close();

        return cloneOutHisto;

    }

    std::vector<float>* fakeFactorTemplate::doFit(TH1F *inSample, TH1F *inQuark, TH1F *inGluon, std::string &titleStr, int debugFlag, int rebinFlag, int rigidFlag){

        inSample->Sumw2();
        inQuark->Sumw2();
        inGluon->Sumw2();

        std::vector<float> *fracOut = new std::vector<float>;

        std::string measurementStr = "test_method_" + titleStr;
        RooStats::HistFactory::Measurement meas(measurementStr.c_str(), measurementStr.c_str());
        meas.SetExportOnly(1);
        meas.SetPOI("fracquark");
        meas.SetLumi(1.0);
        meas.SetLumiRelErr(0.001);
        meas.AddConstantParam("Lumi");

        double startfracquark = 0.5;
        double startfracgluon = 0.5;

        RooStats::HistFactory::Channel chan("channel");
        chan.SetData(inSample);

        inQuark->Scale(inSample->Integral()/inQuark->Integral());
        inGluon->Scale(inSample->Integral()/inGluon->Integral());

        //set zmumu sample
        RooStats::HistFactory::Sample quarksamp("quarksamp");
        quarksamp.SetHisto(inQuark);
        quarksamp.AddNormFactor("fracquark", startfracquark,0.0001, 1);
        quarksamp.SetNormalizeByTheory(false);
	if(rigidFlag == 1){
		quarksamp.ActivateStatError();
	}
        chan.AddSample(quarksamp);

        RooStats::HistFactory::Sample gluonsamp("gluonsamp");
        gluonsamp.SetHisto(inGluon);
        gluonsamp.AddNormFactor("fracgluon", startfracgluon, 0.0001, 1);
        gluonsamp.SetNormalizeByTheory(false);
        if(rigidFlag == 1){
                gluonsamp.ActivateStatError();
        }
	chan.AddSample(gluonsamp);

        meas.AddChannel(chan);
        meas.PrintTree();

        RooWorkspace *space = (RooWorkspace*)RooStats::HistFactory::MakeModelAndMeasurementFast(meas);

        std::string fileStr = "_combined_test_method_" + titleStr + "_model.root";
        TFile *newFile = new TFile(fileStr.c_str());
        RooWorkspace *myspace = (RooWorkspace*)newFile->Get("combined");
        RooStats::ModelConfig* datafit = (RooStats::ModelConfig*)myspace->obj("ModelConfig");
        RooDataSet* data = (RooDataSet*)myspace->data("obsData");
        RooArgSet params(*datafit->GetParametersOfInterest());
                                                                                                                           
        RooArgSet* constrainedParams = datafit->GetPdf()->getParameters(*data);
        RooFit::Constrain(*constrainedParams);
        const RooArgSet* glbObs = datafit->GetGlobalObservables();
        RooAbsReal * nll = datafit->GetPdf()->createNLL(*data, RooFit::Constrain(*constrainedParams), RooFit::GlobalObservables(*glbObs), RooFit::Offset(1) );

        RooMinimizer minim(*nll);
        int status = -99;
        status = minim.minimize("Minuit");
     
        RooFitResult *r = (RooFitResult*)minim.save();

        RooRealVar *firstPOI = (RooRealVar*)datafit->GetParametersOfInterest()->first();

        const RooAbsRealLValue *width = (const RooAbsRealLValue*)datafit->GetObservables()->first();
        
        fracOut->push_back(firstPOI->getValV());
        fracOut->push_back(firstPOI->getError());

        TH1 *modelhisto = (TH1*)datafit->GetPdf()->createHistogram("fithisto",*width);
        TH1F *outHisto = new TH1F(titleStr.c_str(),titleStr.c_str(), 30,0,0.3);
	if(rebinFlag != 1){
		outHisto->Rebin(rebinFlag);
	}  
        modelhisto->Scale(inSample->Integral()/modelhisto->Integral());
        outHisto->Add(modelhisto);

        TFile outFile("fakeTau.root", "UPDATE");
        outHisto->Write();
        outFile.Close();
    
        const char* rm = "rm _results.table";
        
        system(rm);
        
        if(debugFlag == 1){
            
            SetAtlasStyle();
            std::string likelihoodStr = "likelihood.eps";
            std::string likelihoodplotStr = titleStr + "_" + likelihoodStr;
            const char* likelihoodplot = likelihoodplotStr.c_str();
    
            std::string fitStr = "fit.eps";
            std::string fitplotStr = titleStr + "_" + fitStr;
            const char* fitplot = fitplotStr.c_str();
            
            TCanvas *c0 = new TCanvas("c0", "c0", 600,600);
            gStyle->SetOptTitle(0);
            //get likelihood function and plot
            RooRealVar *firstPOI = (RooRealVar *)datafit->GetParametersOfInterest()->first();
            RooPlot* pframe = firstPOI->frame();
            nll->plotOn(pframe,RooFit::ShiftToZero()); 
            pframe->SetMinimum(0);
            pframe->SetMaximum(2800);
            pframe->Draw();
            c0->SaveAs(likelihoodplot);

            const RooAbsRealLValue *width = (const RooAbsRealLValue*)datafit->GetObservables()->first();

            inQuark->Scale(inSample->Integral()/inQuark->Integral());
            inGluon->Scale(inSample->Integral()/inGluon->Integral());

            TCanvas *c1 = new TCanvas ("c1", "c1", 600,600);
            gStyle->SetOptTitle(0);
            TPad *pad11 = new TPad("pad11", "pad11", 0, 0.27, 1, 1.0);
            gStyle->SetOptTitle(0);
            pad11->Draw();
            pad11->cd();
            pad11->SetBottomMargin(0.013);
            inQuark->SetMarkerColor(2);
            inQuark->SetLineColor(2);

	    inSample->SetMarkerStyle(8);
            inQuark->SetMarkerStyle(24);
            inGluon->SetMarkerStyle(25);

            inGluon->SetMarkerColor(3);
            inGluon->SetLineColor(3);
            modelhisto->SetLineColor(kBlue);

            inGluon->Draw("P");
            inSample->Draw("P SAME");
            inQuark->Draw("P SAME");
            modelhisto->Draw("HIST SAME");

            string alphaStr = to_string(firstPOI->getValV());
            string errorStr = to_string(firstPOI->getError());
            string chiStr = to_string(modelhisto->Chi2Test(inSample, "UU CHI2/NDF P"));
            string labelStr = "#alpha_{q} = " + alphaStr;
            string labelStr2 = "#pm" + errorStr;
            string labelStr3 = "#chi^{2}/ndf = " + chiStr;
            const char * label = labelStr.c_str();
            const char * label2 = labelStr2.c_str();
            const char * label3 = labelStr3.c_str();

            inGluon->GetXaxis()->SetTitle("#tau Width");
            inGluon->GetYaxis()->SetTitleOffset(2.25);
            inGluon->GetYaxis()->SetTitle("Events / 0.1");
            inGluon->GetXaxis()->SetTitleSize(0);
            inGluon->GetXaxis()->SetLabelSize(0);
            inGluon->SetMinimum(-0.003);
            myText(       0.65,  0.96, 1, titleStr.c_str());
            ATLASLabel(0.23,0.96,"Internal");
            TLegend *legend1 = new TLegend(0.65,0.70,0.92,0.90);
            myText(       0.67,  0.65, 1, label);
            myText(       0.765,  0.60, 1, label2);
            myText(       0.67,  0.55, 1, label3);
            legend1->AddEntry(inSample, "Sample", "P");
           // legend1->AddEntry(inZmumu, "Z#rightarrow#mu#mu", "P");
            legend1->AddEntry(inQuark, "Quark", "P");
            legend1->AddEntry(inGluon, "Gluon", "P");
            legend1->AddEntry(modelhisto, "Fit", "L");
            legend1->SetShadowColor(0);
            legend1->SetLineColor(0);
            legend1->Draw();
            c1->cd();
            TPad *pad12 = new TPad("pad12", "pad12", 0, 0.05, 1, 0.27);
            gStyle->SetOptTitle(0);
            pad12->SetTopMargin(0.02);
            pad12->SetBottomMargin(0.3);
            pad12->Draw();
            pad12->cd();
    
            TH1F *d22 = (TH1F*)inSample->Clone("h1");
            d22->SetLineColor(kBlack);
            d22->SetMinimum(0.85);
            d22->SetMaximum(1.15);
            d22->Sumw2();
            d22->SetStats(0);      // No statistics on lower plot
            d22->Divide(modelhisto);
            d22->SetMarkerStyle(20);
            d22->Draw("p HIST E0");
    
            d22->GetYaxis()->SetTitle("Mixed/Fit");
            d22->GetYaxis()->SetNdivisions(505);
            d22->GetYaxis()->SetTitleSize(18);
            d22->GetYaxis()->SetTitleFont(43);
            d22->GetYaxis()->SetTitleOffset(1.55);
            d22->GetYaxis()->SetLabelFont(43);
            d22->GetYaxis()->SetLabelSize(18);
            d22->GetXaxis()->SetTitleSize(18);
            d22->GetXaxis()->SetTitleFont(43);
            d22->GetXaxis()->SetTitleOffset(4.);
            d22->GetXaxis()->SetLabelFont(43);
            d22->GetXaxis()->SetLabelSize(18);
            d22->GetXaxis()->SetTitle("Jet Width");
            TLine *lineh1 = new TLine(0,1,0.3,1);
            lineh1->Draw();
            gPad->Update();
            gPad->Modified();

            c1->SaveAs(fitplot);
            
        }
        
        return fracOut;

    }

    std::vector<std::vector<float>>* fakeFactorTemplate::getQuarkFractions(std::vector<TH1F*> *inZmumu, std::vector<TH1F*> *inMultijet, std::vector<TH1F*> *inRegion, std::vector<std::vector<TH1F*>> *inTemplate, double *bins, int &numBins, std::string &prong, int rebinFlag, int rigidFlag){
        
        // do a fit for input samples to quark/gluon templates

        std::vector<TH1F> fitHistoZmumu;
        std::vector<TH1F> fitHistoMultijet;
        std::vector<TH1F> fitHistoSample;
        std::vector<float> fracZmumu;
        std::vector<float> fracMultijet;
        std::vector<float> fracSample;
        std::vector<float> errorZmumu;
        std::vector<float> errorMultijet;
        std::vector<float> errorSample;
        std::vector<std::vector<float>> *outFractions = new std::vector<std::vector<float>>;

        for(int i = 0; i < numBins; i++){
            minBin = bins[i];
            maxBin = bins[i + 1];
            minInt = minBin;
            maxInt = maxBin;
            minStr = std::to_string(minInt);
            maxStr = std::to_string(maxInt);
            titleZmumuStr = "quark_frac_" + minStr + maxStr + "_" + prong + "_zmumu_fit";
            titleMultijetStr = "quark_frac_" + minStr + maxStr + "_" + prong + "_multijet_fit";
            titleSampleStr = "quark_frac_" + minStr + maxStr + "_" + prong + "_sample_fit";

            float zeros = inRegion->at(i)->Integral();

            if(zeros == 0){
                std::cout << "Skipping empty histogram for: " << prong << ": " << minStr << "-" << maxStr << std::endl;
                fracSample.push_back(-999);
                errorSample.push_back(-999);
            }

            if(zeros != 0){
            	if(rebinFlag == 1){
	    		std::vector<float> *inZmumuFrac = doFit(inZmumu->at(i), inTemplate->at(0).at(i), inTemplate->at(1).at(i), titleZmumuStr, rebinFlag, rigidFlag);

            		fracZmumu.push_back(inZmumuFrac->at(0));
            		errorZmumu.push_back(inZmumuFrac->at(1));
	    	}
	    	if(rebinFlag != 1){
			inZmumu->at(i)->Rebin(rebinFlag);
			inTemplate->at(0).at(i)->Rebin(rebinFlag);
			inTemplate->at(1).at(i)->Rebin(rebinFlag);

                	std::vector<float> *inZmumuFrac = doFit(inZmumu->at(i), inTemplate->at(0).at(i), inTemplate->at(1).at(i), titleZmumuStr, rebinFlag, rigidFlag);

                	fracZmumu.push_back(inZmumuFrac->at(0));
                	errorZmumu.push_back(inZmumuFrac->at(1));
	    	}

            	TH1F *tempMultijet = new TH1F("temp", "temp", 30, 0, 0.3);
            	tempMultijet->Sumw2();

            	for(int mjet = 1; mjet < 31; mjet++){
                	tempMultijet->SetBinContent(mjet, inMultijet->at(i)->GetBinContent(mjet));
                	tempMultijet->SetBinError(mjet, inMultijet->at(i)->GetBinError(mjet));
            	}

	    	if(rebinFlag == 1){
            		std::vector<float> *inMultijetFrac = doFit(tempMultijet, inTemplate->at(2).at(i), inTemplate->at(3).at(i), titleMultijetStr, rebinFlag, rigidFlag);

            		fracMultijet.push_back(inMultijetFrac->at(0));
            		errorMultijet.push_back(inMultijetFrac->at(1));
	    	}

            	if(rebinFlag != 1){
			tempMultijet->Rebin(rebinFlag);
			inTemplate->at(2).at(i)->Rebin(rebinFlag);
			inTemplate->at(3).at(i)->Rebin(rebinFlag);

                	std::vector<float> *inMultijetFrac = doFit(tempMultijet, inTemplate->at(2).at(i), inTemplate->at(3).at(i), titleMultijetStr, rebinFlag, rigidFlag);

                	fracMultijet.push_back(inMultijetFrac->at(0));
                	errorMultijet.push_back(inMultijetFrac->at(1));
            	}

			std::cout << "Running fit for: " << prong << ": " << minStr << "-" << maxStr << std::endl;
            	
            	if(rebinFlag == 1){
			std::vector<float> *inSampleFrac = doFit(inRegion->at(i), inTemplate->at(4).at(i), inTemplate->at(5).at(i), titleSampleStr, rebinFlag, rigidFlag);
			fracSample.push_back(inSampleFrac->at(0));
            		errorSample.push_back(inSampleFrac->at(1));
		}
		if(rebinFlag != 1){
			inRegion->at(i)->Rebin(rebinFlag);
			inTemplate->at(4).at(i)->Rebin(rebinFlag);
			inTemplate->at(5).at(i)->Rebin(rebinFlag);

                        std::vector<float> *inSampleFrac = doFit(inRegion->at(i), inTemplate->at(4).at(i), inTemplate->at(5).at(i), titleSampleStr, rebinFlag, rigidFlag);
                        fracSample.push_back(inSampleFrac->at(0));
                        errorSample.push_back(inSampleFrac->at(1));
		}			
	    }
        }

        outFractions->push_back(fracZmumu);
        outFractions->push_back(fracMultijet);
        outFractions->push_back(fracSample);
        outFractions->push_back(errorZmumu);
        outFractions->push_back(errorMultijet);
        outFractions->push_back(errorSample);
        
        return outFractions;
        
    }

    void fakeFactorTemplate::interpolateFF(std::vector<TH1F*>* inZmumuFF, std::vector<TH1F*>* inMultijetFF, std::vector<std::vector<float>> *quarkFractions, double *bins, int &numBins, std::string &prong, int debugFlag){

        titleStr = prong + "FF";

        TH1F *FF = new TH1F(titleStr.c_str(), titleStr.c_str(), numBins, bins);

        for(int i = 0; i < numBins; i++){
            zmumuFF = inZmumuFF->at(i)->GetBinContent(1);
            multijetFF = inMultijetFF->at(i)->GetBinContent(1);
            zmumuQuarkFrac = quarkFractions->at(0).at(i);
            multijetQuarkFrac = quarkFractions->at(1).at(i);
            sampleQuarkFrac = quarkFractions->at(2).at(i);
	    if(sampleQuarkFrac == -999) continue;
            quarkFF = (((1 + multijetQuarkFrac)*zmumuFF) - ((1 + zmumuQuarkFrac)*multijetFF))/(multijetQuarkFrac - zmumuQuarkFrac);
            gluonFF = ((multijetQuarkFrac*zmumuFF) - (zmumuQuarkFrac*multijetFF))/(multijetQuarkFrac - zmumuQuarkFrac);
            outFF = (sampleQuarkFrac*quarkFF) + ((1-sampleQuarkFrac)*gluonFF);
            FF->SetBinContent(i + 1, outFF);
        }

        TFile outFile("fakeTau.root", "UPDATE");
        FF->Write();
        outFile.Close();

	if(debugFlag == 1){
	    SetAtlasStyle();
	    TCanvas *c1 = new TCanvas ("c1", "c1", 600,600);
            FF->SetLineColor(kBlue);

            FF->Draw("HIST SAME");

            FF->GetXaxis()->SetTitle("#tau p_{T}");
            FF->GetYaxis()->SetTitleOffset(2.25);
            FF->GetYaxis()->SetTitle("FF");
            myText(       0.65,  0.96, 1, titleStr.c_str());
            ATLASLabel(0.23,0.96,"Internal");
            TLegend *legend1 = new TLegend(0.80,0.80,0.90,0.90);
           
            legend1->AddEntry(FF, "FF", "L");
            legend1->SetShadowColor(0);
            legend1->SetLineColor(0);
            legend1->Draw();
	
  	    std::string outTitle = titleStr + ".eps";
	    c1->SaveAs(outTitle.c_str());
	}
    }
    
    std::vector<std::string>* fakeFactorTemplate::getDirectories(const char* inFile){

        std::vector<std::string>* outDirectories = new std::vector<std::string>;

        TFile *file = new TFile(inFile);
        TIter next(gDirectory->GetListOfKeys());
        TKey *key;

        while((key = (TKey*)next())) {
            outDirectories->push_back(std::string(key->GetTitle()));
        }

        return outDirectories;

    }

    std::vector<std::string>* fakeFactorTemplate::getCuts(std::string &inDirectory){

        std::vector<std::string>* outCuts = new std::vector<std::string>;

        directory = inDirectory.c_str();

        std::vector<char> temp;

	int directorysize = strlen(directory);
        for(int i = 0; i < directorysize; i++){
            if(directory[i] != '_'){
                temp.push_back(directory[i]);
            }
            if((directory[i] == '_') || (i == (directorysize-1))){
                std::string tempStr;
		int tempsize = temp.size();
                for(int j = 0; j < tempsize; j++){
                    tempStr = tempStr + temp.at(j);
                }
                outCuts->push_back(tempStr);
                temp.clear();
            }
        }

        return outCuts;

    }

    std::vector<int> fakeFactorTemplate::getBinning(std::string &inDirectory, const char* inFile){

        TFile *file = new TFile(inFile);

        std::vector<int> outBins;

        directory = inDirectory.c_str();
        std::vector<char> temp;

        file->cd(directory);
        TKey *iterate_key;
        TIter newnext(gDirectory->GetListOfKeys());

        while((iterate_key = (TKey*)newnext())){
            const char* tempChar =  iterate_key->GetName();
            int charsize = strlen(tempChar);
	    for(int i = 4; i < charsize; i++){
                if(tempChar[i] == '5') continue;
                if((tempChar[i] == '9') && (tempChar[i-1] == '9') && (tempChar[i-2] == '9')) continue;
                if((tempChar[i] != '_') && (tempChar[i] != '0')){
                    temp.push_back(tempChar[i]);
                }
                if(tempChar[i] == '_') break;
            }
	    int tempsize = temp.size();
            for(int i = 0; i < tempsize; i++){
                if((i == (tempsize - 2)) && (temp.at(i) == '9') && (temp.at(i + 1) == '9')) continue;
                if((i == (tempsize - 1)) && (temp.at(i) == '9') && (temp.at(i - 1) == '9')){
                    outBins.push_back(999);
                }
                if(temp.at(i) == '1'){
                    outBins.push_back(150);
                }
                else{
                    int val = temp.at(i) - '0';
                    int outValFinal = val * 10;
                    if((outValFinal == 20) || (outValFinal == 30) || (outValFinal == 40) || (outValFinal == 60) || (outValFinal == 90) || (outValFinal == 150) || (outValFinal == 999)){
                        outBins.push_back(outValFinal);
                    }
                }
            }
            temp.clear();
        }

        return outBins;

    }

    void fakeFactorTemplate::getFakeFactors(const char* SampleFileName, int debugFlag, int rebinFlag, int rigidFlag){

        // coordinate functions

        std::cout<< "////////////////////////////////////////" <<std::endl;
        std::cout<< "///////////////////////////////////////" <<std::endl;
        std::cout<< "///////         //// ///         //////" <<std::endl;
        std::cout<< "//////////  / /////// /////  //////////" <<std::endl;
        std::cout<< "//////////  ////       ////  //////////" <<std::endl;
        std::cout<< "////// ///  ///////// /////  /// //////" <<std::endl;     
        std::cout<< "//////      //////// //////      //////" <<std::endl;
        std::cout<< "///////////////////////////////////////" <<std::endl;
        std::cout<< "//                                   //" <<std::endl;
        std::cout<< "//    ATLAS Fake Tau Tool            //" <<std::endl;
        std::cout<< "//    Date: 05/05/2019               //" <<std::endl;
        std::cout<< "//    Author: Joe Muse               //" <<std::endl;
        std::cout<< "//    Email: joseph.m.muse@ou.edu    //" <<std::endl;
        std::cout<< "//                                   //" <<std::endl;
        std::cout<< "///////////////////////////////////////" <<std::endl;  

        Zmumu = "Zmumu";
        Multijet = "Multijet";
        pt = "pt";
        width = "width";
        ff = "FF";
        Wlnu = "Wlnu";
        Zll = "Zll";
        jetjet = "jetjet";
        ZmumuFileName = "/eos/user/j/jmuse/fakeToolInputs/Zmumu_inputs_for_technical_implementation_190323.root";
        MultijetFileName = "/eos/user/j/jmuse/fakeToolInputs/Multijet_inputs_280319.root";
        WlnuFileName = "/eos/user/j/jmuse/fakeToolInputs/MC_input_mc16d_PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wlnu.root";
        ZllFileName = "/eos/user/j/jmuse/fakeToolInputs/MC_input_mc16d_PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zll.root";
        jetjetFileName = "/eos/user/j/jmuse/fakeToolInputs/MC_input_mc16d_Pythia8EvtGen_A14NNPDF23LO_jetjet.root";

        std::vector<const char*> *inFileNames = new std::vector<const char*>;
        std::vector<std::string> samples;

        inFileNames->push_back(WlnuFileName);
        inFileNames->push_back(ZllFileName);
        inFileNames->push_back(jetjetFileName);

        samples.push_back(Wlnu);
        samples.push_back(Zll);
        samples.push_back(jetjet);

        std::vector<std::string> *directories = getDirectories(SampleFileName);

        std::vector<double> *scaleVector = getLumiScale(inFileNames);

	int directorysize = directories->size();
        for(int i = 0; i < directorysize; i++){

            std::vector<std::string>* cuts = getCuts(directories->at(i));
            std::string Sample = cuts->at(0);
            std::string prong = cuts->at(1);
            std::vector<int> binning = getBinning(directories->at(i), SampleFileName);
            sort(binning.begin(), binning.end());
            binning.erase(unique(binning.begin(), binning.end()), binning.end());

            int size = binning.size();
            numBins = size - 1;
            double bins[numBins];
            std::copy(binning.begin(), binning.end(), bins);

            std::cout << "Running: " + prong << std::endl;
            
            std::cout << "Running: zmumuPTHisto" << std::endl;
            std::vector<TH1F*> *zmumuPTHisto = getHisto(bins, numBins, cuts, pt, Zmumu, ZmumuFileName, debugFlag);
            
            std::cout << "Running: multijetPTHisto" << std::endl;
            std::vector<TH1F*> *multijetPTHisto = getHisto(bins, numBins, cuts, pt, Multijet, MultijetFileName, debugFlag);

            std::cout << "Running: samplePTHisto" << std::endl;
            std::vector<TH1F*> *samplePTHisto = getHisto(bins, numBins, cuts, pt, Sample, SampleFileName, debugFlag);

            std::cout << "Running: zmumuWidthHisto" << std::endl;
            std::vector<TH1F*> *zmumuWidthHisto = getHisto(bins, numBins, cuts, width, Zmumu, ZmumuFileName, debugFlag);
            
            std::cout << "Running: multijetWidthHisto" << std::endl;
            std::vector<TH1F*> *multijetWidthHisto = getHisto(bins, numBins, cuts, width, Multijet, MultijetFileName, debugFlag);

            std::cout << "Running: sampleWidthHisto" << std::endl;
            std::vector<TH1F*> *sampleWidthHisto = getHisto(bins, numBins, cuts, width, Sample, SampleFileName, debugFlag);

            std::cout << "Running: zmumuFFHisto" << std::endl;
            std::vector<TH1F*> *zmumuFFHisto = getHisto(bins, numBins, cuts, ff, Zmumu, ZmumuFileName, debugFlag);
            
            std::cout << "Running: multijetFFHisto" << std::endl;
            std::vector<TH1F*> *multijetFFHisto = getHisto(bins, numBins, cuts, ff, Multijet, MultijetFileName, debugFlag);

            std::cout << "Running: WlnuPTHisto" << std::endl;
            std::vector<std::vector<TH1F*>> *WlnuPTHisto = getPTDistro(Wlnu, bins, numBins, scaleVector, cuts, WlnuFileName, debugFlag);

            std::cout << "Running: ZllPTHisto" << std::endl;
            std::vector<std::vector<TH1F*>> *ZllPTHisto = getPTDistro(Zll, bins, numBins, scaleVector, cuts, ZllFileName, debugFlag);

            std::cout << "Running: JetjetPTHisto" << std::endl;
            std::vector<std::vector<TH1F*>> *JetjetPTHisto = getPTDistro(jetjet, bins, numBins, scaleVector, cuts, jetjetFileName, debugFlag);

            std::cout << "Running: zmumuPTNormalizationWlnu" << std::endl;
            std::vector<std::vector<TH1F*>> *zmumuPTNormalizationWlnu = getPTNormalization(zmumuPTHisto, WlnuPTHisto, bins, numBins, prong, Wlnu, Zmumu, debugFlag);

            std::cout << "Running: zmumuPTNormalizationZll" << std::endl;
            std::vector<std::vector<TH1F*>> *zmumuPTNormalizationZll = getPTNormalization(zmumuPTHisto, ZllPTHisto, bins, numBins, prong, Zll, Zmumu, debugFlag);

            std::cout << "Running: zmumuPTNormalizationJetjet" << std::endl;
            std::vector<std::vector<TH1F*>> *zmumuPTNormalizationJetjet = getPTNormalization(zmumuPTHisto, JetjetPTHisto, bins, numBins, prong, jetjet, Zmumu, debugFlag);

            std::cout << "Running: multijetPTNormalizationWlnu" << std::endl;
            std::vector<std::vector<TH1F*>> *multijetPTNormalizationWlnu = getMultijetPTNormalization(multijetPTHisto, WlnuPTHisto, bins, numBins, prong, Wlnu, debugFlag);

            std::cout << "Running: multijetPTNormalizationZll" << std::endl;
            std::vector<std::vector<TH1F*>> *multijetPTNormalizationZll = getMultijetPTNormalization(multijetPTHisto, ZllPTHisto, bins, numBins, prong, Zll, debugFlag);

            std::cout << "Running: multijetPTNormalizationJetjet" << std::endl;
            std::vector<std::vector<TH1F*>> *multijetPTNormalizationJetjet = getMultijetPTNormalization(multijetPTHisto, JetjetPTHisto, bins, numBins, prong, jetjet, debugFlag);

            std::cout << "Running: samplePTNormalizationWlnu" << std::endl;
            std::vector<std::vector<TH1F*>> *samplePTNormalizationWlnu = getPTNormalization(samplePTHisto, WlnuPTHisto, bins, numBins, prong, Wlnu, Sample, debugFlag);

            std::cout << "Running: samplePTNormalizationZll" << std::endl;
            std::vector<std::vector<TH1F*>> *samplePTNormalizationZll = getPTNormalization(samplePTHisto, ZllPTHisto, bins, numBins, prong, Zll, Sample, debugFlag);

            std::cout << "Running: samplePTNormalizationJetjet" << std::endl;
            std::vector<std::vector<TH1F*>> *samplePTNormalizationJetjet = getPTNormalization(samplePTHisto, JetjetPTHisto, bins, numBins, prong, jetjet, Sample, debugFlag);

            std::cout << "Running: WlnuWidthHisto" << std::endl;
            std::vector<std::vector<TH1F*>> *WlnuWidthHisto = getWidthDistro(zmumuPTNormalizationWlnu, multijetPTNormalizationWlnu, samplePTNormalizationWlnu, cuts, Wlnu, bins, numBins, scaleVector, WlnuFileName, debugFlag);

            std::cout << "Running: ZllWidthHisto" << std::endl;
            std::vector<std::vector<TH1F*>> *ZllWidthHisto = getWidthDistro(zmumuPTNormalizationZll, multijetPTNormalizationZll, samplePTNormalizationZll, cuts, Zll, bins, numBins, scaleVector, ZllFileName, debugFlag);

            std::cout << "Running: JetjetWidthHisto" << std::endl;
            std::vector<std::vector<TH1F*>> *JetjetWidthHisto = getWidthDistro(zmumuPTNormalizationJetjet, multijetPTNormalizationJetjet, samplePTNormalizationJetjet, cuts, jetjet, bins, numBins, scaleVector, jetjetFileName, debugFlag);

            std::cout << "Running: templatesWidthHistoCombined" << std::endl;
            std::vector<std::vector<TH1F*>> *templatesWidthHistoCombined = combineHistos(WlnuWidthHisto, ZllWidthHisto, JetjetWidthHisto, bins, numBins, prong, debugFlag);

            std::cout << "Running: quarkFractionInputTemplates" << std::endl;
            std::vector<std::vector<TH1F*>> *quarkFractionInputTemplates = combineGluonUnmatchedHistos(templatesWidthHistoCombined, bins, numBins, prong, debugFlag);

            std::cout << "Running: quarkFractions" << std::endl;
            std::vector<std::vector<float>> *quarkFractions = getQuarkFractions(zmumuWidthHisto, multijetWidthHisto, sampleWidthHisto, quarkFractionInputTemplates, bins, numBins, prong, rebinFlag, rigidFlag);

            std::cout << "Running: interpolateFF" << std::endl;
            interpolateFF(zmumuFFHisto, multijetFFHisto, quarkFractions, bins, numBins, prong);

            delete zmumuPTHisto;
            delete multijetPTHisto;
            delete samplePTHisto;
            delete zmumuWidthHisto;
            delete multijetWidthHisto;
            delete sampleWidthHisto;
            delete zmumuFFHisto;
            delete multijetFFHisto;
            delete WlnuPTHisto;
            delete ZllPTHisto;
            delete JetjetPTHisto;
            delete zmumuPTNormalizationWlnu;
            delete zmumuPTNormalizationZll;
            delete zmumuPTNormalizationJetjet;
            delete multijetPTNormalizationWlnu;
            delete multijetPTNormalizationZll;
            delete multijetPTNormalizationJetjet;
            delete samplePTNormalizationWlnu;
            delete samplePTNormalizationZll;
            delete samplePTNormalizationJetjet;
            delete WlnuWidthHisto;
            delete ZllWidthHisto;
            delete JetjetWidthHisto;
            delete templatesWidthHistoCombined;
            delete quarkFractionInputTemplates;
            delete quarkFractions;

        }   
        
        std::cout << "All Done!" << std::endl;

    }
}
