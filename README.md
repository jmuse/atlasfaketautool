# ATLAS Tau Fake Factor Tool

**Please note: The tool is not ready for use and is not a current recommendation 
of TauWG!**

The Tau Fake Factor Tool is an effort to harmonize estimation of jets 
reconstructed as tau objects, born from the Fake Tau Task Force (FTTF). The tool 
calculates fake factors (FF) for jets that are misidentified as taus. This is 
achieved by identifying the fraction of fake tau jets initiated by quarks and 
interpolating FF between known FFs associated with Z+jets and multijet processes
for bins of pT and number of prongs. Therefore, the tool provides FFs for Z+jets
and multijet regions as well as MC templates for quark and gluon initated jets 
in order to conduct the fit for the quark fraction. The discriminating variable 
used for quark/gluon separation is the LCTopo jet width.
An alternate method of the tool is available for use, which finds the fraction 
of multijet-like events in a given sample (LINK)

# Usage

The Tau Fake Factor Tool (TFFT) exists as a single executable in the Athena 21.2 
framework. The nominal tool requires a specific directory structure to specify 
the region where FF is calculated. In order to accommodate full functionality, 
each directory (each region), should include pT and jet width distributions 
binned in supported pT bins. The tool will parse the directory structure and 
perform interpolation “on-the-fly”. The workflow of TFFT is shown in attachment 
below ("workflow").

Note, the FTTF has not provided recommendations for systematics at this time. 
The tool, subsequently, does not provide systematics.

TFFT’s single executable (named fakeTauTool) requires one argument with an 
option flag:

`$ fakeTauTool \path\to\input\file.root debugFlag = false -rebin 1 -rigidFlag 0`

The **debugFlag** is an option argument (set to “false” by default). When set to 
“true”, provides every available histogram used by the tool for every step 
(including pT re-weighting to the input sample and those provided by the tool) 
as well as stacked histograms showing the quark fraction fit result against the 
quark/gluon templates. When set to the default, “false”, the tool simply 
provides the quark fraction fit histograms and interpolated FF.

The **rebin** option allows rebinning of width histograms (set to "1" by default,
which corresponds to no rebinning). The argument corresponds to the number of 
bins to merge. For instance, setting to "2", will take 30 bins and merge into
15.

The **rigidFlag** option introduces statistical uncertainty of the MC quark and gluon 
templates as a constraint term in the fit. The default is "0", which corresponds to rigid
templates whose statistical fluctuation will not influence the fit.

# Required Input Structure

A specific directory naming structure of inputs to TFFT is required in order to 
match specifics about the region of interest with the appropriate interpolation 
region. An example is to interpolate FF with Z+jets and multijet regions with 
the same BDT working point as the sample of interest.

The naming scheme for directories are:

`SampleName_Prong_BDTWP_WPpassORfail_BDTScoreCut_JvtCut_TriggerPass`

with the following options (* refers to currently available options):

SampleName | Prong | BDTWP | WPpassORfail | BDTScoreCut | JvtCut | TriggerPass
---------- | ----- | ----- | ------------ | ----------- | ------ | -----------
"Arbitrary"	| 1p* | loose | pass* | bdt005* | jvt0* | trig0*
 | 3p* | medium* | fail* | bdt05 | jvt1 | trig1
 | | tight | | | |

**SampleName** is a user-defined identifier which can be any arbitrary 
alphanumeric sequence. **Prong** is the prongness of the sample (one or three 
prong). **BDTWP** is the BDT working point of interest. **WPpassORfail** is 
whether the sample is the BDT pass or fail region. **BDTScoreCut** corresponds 
to a BDT score cut in the sample. **JvtCut** is whether tau objects have a JVT 
cut applied or not. **TriggerPass** is whether the tau objects are 
trigger-matched to a tau trigger in the sample or not. As an example, consider a 
sample of interest which contains one prong tau objects that fail the medium BDT 
working point, with BDT score cut of 0.005 with no JVT cut and are not subjected 
to trigger-matching.

The directory should be named:

`mysample_1p_medium_fail_bdt005_jvt0_trig0`

Each directory should contain histograms of pT and jet width. The histogram 
naming convention defines the pT bins for which the user wishes to interpolate 
FF.

The naming scheme for histograms is:

`h_ptMinMax_pt`

`h_ptMinMax_width`

This implies each pT bin should have a pT and jet width histogram. The available 
bins are:

Min | Max
--- | ---
20 | 30
30 | 40
40 | 60
60 | 90
90 | 150
150 | 999

Thus, if one was interested in interpolating FF in pT bins 20-30 and 30-40, the 
following histograms should be included inside the appropriate directory:

`h_pt2030_pt`

`h_pt2030_width`

`h_pt3040_pt`

`h_pt3040_width`

The included histograms must be binned in order for direct comparison to 
provided templates. pT bins must be “fine-binned” in intervals of 1 GeV, where 
jet width histograms should have 30 bins from 0-0.3.

For instance, given the 20-30 GeV pT bin:

* pT binning:

`{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30}`

* jet width binning:

`{0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 
0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 
0.26, 0.27, 0.28, 0.29, 0.3}`

Once the appropriate histograms are created and placed into the appropriate 
directory, the resulting ROOT file can be given directory to TFFT. The tool has 
the capability to handle as many different samples (directories) at once as 
desired.

# Test Input

A sample file which can be used for testing the functionality of the tool is 
available. The sample file is a bin-by-bin 40/60 mixture of multijet/Z+jet 
interpolation regions with pT bins up to 150 GeV.

The file is located at /eos/user/j/jmuse/fakeToolInputs

# Contact

Feel free to contact Joe Muse at joseph.m.muse@ou.edu with any 
questions/concerns/comments about TFFT.

# Documentation

Internal note:

Twiki: 
